<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
   xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
   xmlns="http://www.opengis.net/sld" 
   xmlns:ogc="http://www.opengis.net/ogc" 
  xmlns:xlink="http://www.w3.org/1999/xlink" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>Simple Point</Name>
    <UserStyle>
      <Title>SLD Cook Book: Simple Point</Title>
      <FeatureTypeStyle>
      	<Rule>
          <ogc:Filter>
            <ogc:PropertyIsNull >
              <ogc:PropertyName>tree_status</ogc:PropertyName>
            </ogc:PropertyIsNull >
          </ogc:Filter>
          <MaxScaleDenominator>17062</MaxScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#6ae377</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>4.5</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsNull >
              <ogc:PropertyName>tree_status</ogc:PropertyName>
            </ogc:PropertyIsNull >
          </ogc:Filter>
          <MinScaleDenominator>17062</MinScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#6ae377</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>20</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>INCOMPLETE</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>17062</MaxScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#6ae377</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>4.5</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>INCOMPLETE</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>17062</MinScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#6ae377</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>20</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>DRAFT</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>17062</MaxScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#019D57</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#6ae377</CssParameter>
              		<CssParameter name="stroke-width">1</CssParameter>
                </Stroke>
              </Mark>
              <Size>4.5</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>DRAFT</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>17062</MinScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#019D57</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#6ae377</CssParameter>
              		<CssParameter name="stroke-width">1</CssParameter>
                </Stroke>
              </Mark>
              <Size>20</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>VALIDATED</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>17062</MaxScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#019D57</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>4.5</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>tree_status</ogc:PropertyName>
              <ogc:Literal>VALIDATED</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>17062</MinScaleDenominator>
          <PointSymbolizer uom="http://www.opengeospatial.org/se/units/metre">
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#019D57</CssParameter>
                </Fill>
                                <Stroke>
              		<CssParameter name="stroke">#ffffff</CssParameter>
              		<CssParameter name="stroke-opacity">0.1</CssParameter>
              		<CssParameter name="stroke-width">0.5</CssParameter>
                </Stroke>
              </Mark>
              <Size>4.5</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>        
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>