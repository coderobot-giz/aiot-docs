# Gamificación

El objetivo y narrativa de la estrategia de gamificación de Árbol IoT es la de transformar al usuario en un experto en el arbolado urbano, comprometido y consciente de la importancia del mismo como mecanismo de adaptación al cambio climático. 

Para lograr dicha meta el usuario atravesará la ruta de los árboles icónicos ancestrales; un total de cinco niveles que acompañarán al participante del nivel Bugambilia al nivel Sequoia, donde cada interacción y contribución a la aplicación permitirán al mismo ganar conocimiento, experiencia y reconocimiento social además del sentido de comunidad y cuidado por el medio ambiente. 

Para esto se contemplaron los siguientes componentes de gamificación los cuales responden a las motivaciones intrínsecas y extrínsecas de los usuarios:

### Puntos

Se le otorga al usuario puntos por cada tarea y acción realizada en Árbol IoT entre las que destacan las siguientes:

- Ingresar a la app (2 puntos)
- Tomar tutorial (4 puntos)
- Capturar un árbol (5 puntos)
- Editar un árbol (3 puntos)
- Adoptar árbol (5 puntos)
- Actualizar foto estacional (10 puntos)
- Regar un árbol adoptado (3 puntos)
- Levantar un hilo (foro) (1 punto)
- Responder a un hilo (3 puntos)
- Contestar pregunta correcta en trivias (2 puntos)
- Elaborar trivia (25 puntos)
- Agregar amigo (1 punto)

#### Niveles

El usuario podrá avanzar de nivel a través de la acumulación de puntos, a continuación se detallan los puntos requeridos por cada nivel:

- Bugambilia (0 a 15)
- Ficus (15 a 45)
- Roble (45 a 200)
- Baobab (200 a 400)
- Seqüoya (400+)

#### Insignias 

Al realizar ciertas tareas o acciones el usuario podrá obtener insignias relacionadas, para más detalles revisar la siguiente figura:

![Screenshot](../images/img-17.png)

### Retroalimentación

Complementario a las insignias y niveles, la estrategia incorpora la mecánica de la retroalimentación, con barras de progreso y a través de reportes de la contribución del usuario al medio ambiente.

### Tabla de líderes

Se contemplan también rankings globales y de amigos, para fomentar la conexión y competencia entre usuarios.

### Adopción de un árbol

Los usuarios al capturar un nuevo árbol o al editar alguno incompleto podrán adoptar dicho árbol para posteriormente regarlo y cuidar de él.

### Regado del árbol digital / Actualización de foto estacional

Los árboles adoptados por el usuario podrán ser regados digitalmente y podrán recibir actualizaciones de su foto estacional.

### Trivias

Se contempla también un módulo trivia con preguntas y respuestas relacionadas al medio ambiente y el arbolado urbano.