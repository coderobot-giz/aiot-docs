# Instalación

En este apartado se detalla el proceso de instalación de la solución tecnológica Árbol IoT, 
se recomienda la utilización de Ubuntu Server 18.04 o superior como sistema operativo base, 
siendo esta versión la utilizada durante la creación de este manual.

Para la instalación de Árbol IoT se requiere del conocimiento de las tecnologías mencionadas
en la arquitectura de la solución. Se recomienda la ejecución de los comandos con una cuenta 
de usuario habilitada para ejecutar sudo, anteponiendo el comando sudo a los comandos que así
lo requieran, alternativamente también puede ejecutarse la guía con una cuenta de root.

## Dependencias

### Instalar docker y docker compose

#### Docker

Primero, actualizar listado de paquetes de Ubuntu Server:
```
apt update
```

A continuación, instalar dependencias que nos permitan usar paquetes a través de HTTPS:
```
apt install apt-transport-https ca-certificates curl software-properties-common gnupg
```

Luego, agregue la clave GPG para el repositorio oficial de Docker a su sistema:
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Ahora, agregar el repositorio Docker a las fuentes APT:
```
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
```

A continuación, actualizar la base de datos de paquetes con los paquetes de Docker desde el repositorio recién agregado:
```
apt update
```
Ahora, instalar Docker:
```
apt install docker-ce
```
Docker ahora debería estar instalado, el demonio iniciado y el proceso habilitado para iniciarse en el arranque. 
Comprobar que se está ejecutando:
```
systemctl status docker
```

Instalar Docker no solo brinda el servicio(daemon) sino también la utilidad de línea de comando de docker o el cliente Docker.

#### Docker compose

Primero, ejecutar el siguiente comando para descargar la versión estable de Docker Compose:
```
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Luego, aplicar permisos ejecutables al binario:
```
chmod +x /usr/local/bin/docker-compose
```
Entonces, crear enlace simbólico a /usr/bin 
```
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
Probar la instalación
```
docker-compose --version
```
Para más detalles o resolución de problemas visitar:

[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Árbol IoT Web

Para instalar Árbol IoT Web, realizar los pasos descritos a continuación:

Primero, crear directorio de instalación 
```
mkdir -p /var/lib/aiot
```
Acceder al nuevo directorio
```
cd /var/lib/aiot
```
Clonar repositorio con documentación y archivos de configuración
```
git clone https://gitlab.com/coderobot-giz/aiot-docs.git
mv aiot-docs/* ./
rm -r aiot-docs
```
### Preparar configuración de MapTiles

Descargar capa base de la región en la cual se desea realizar la instalación de la solución,
acceder a la siguiente URL para la descarga:

[https://openmaptiles.com/downloads/dataset/osm/#0.23/0/-26](https://openmaptiles.com/downloads/dataset/osm/#0.23/0/-26)

Realizar los siguientes pasos:

![Screenshot](../images/img-2.png)

Copiar el comando de descarga generado:

![Screenshot](../images/img-2.1.png)

Una vez copiado al portapapeles, pegar la línea de descarga en la terminal:

```
wget -c url-generada-por-openmaptiles.com
```


Una vez descargado el archivo, mover el mismo a la carpeta de trabajo del tileserver:

```
mv osm* /var/lib/aiot/prod/tileserver/data/map.mbtiles
```

### Preparar certificados HTTPS

Árbol IoT Web y Móvil requieren obligatoriamente de certificados HTTPS para funcionar, realizar los pasos descritos a continuación para obtenerlos:

#### Entorno local

Solo para pruebas, ejecutar los siguientes comandos:

```
mkdir -p /var/lib/aiot/prod/nginx/cert
cd /var/lib/aiot/prod/nginx/cert
openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=localhost" -keyout ./privkey.pem -out ./fullchain.pem
```

#### Entorno productivo

Para el desplegado en el entorno productivo, obtener privkey.pem  y fullchain.pem de su proveedor de certificados y ubicarlos en el siguiente directorio:

```
mkdir -p /var/lib/aiot/prod/nginx/cert
/var/lib/aiot/prod/nginx/cert
```

#### Consideraciones

Es importante mantener el nombre de los archivos privkey.pem y fullchain.pem respectivamente para que estos puedan ser leídos correctamente.


### Entorno y variables de entorno

El entorno de Árbol IoT se construye a partir de un descriptor YML de docker compose, antes de iniciar el backend es necesario definir dicho archivo YML y sus
configuraciones, realizar los pasos descritos a continuación:

Primero, crear archivo docker-compose.yml

```
cd /var/lib/aiot
nano ./docker-compose.yml  
```

Asegurarse de que el contenido del nuevo archivo sea el siguiente:

```yaml
version: "3"

services:

  nginx:
    image: nginx
    volumes:
      -  ./prod/nginx/nginx.conf:/etc/nginx/nginx.conf:ro 
      -  ./prod/nginx/cert:/etc/nginx/cert
      -  ./prod/nginx/cache:/nginx-cache
    ports:
     - "80:80" 
     - "443:443"
    depends_on:
     - postgres
     - minio
     - openmaptiles
     - geoserver
     - arboliot

  postgres:
    image: mdillon/postgis:9.6-alpine
    volumes:
      - ./prod/postgresql/:/var/lib/postgresql/data/ 
    ports:
     - "127.0.0.1:5432:5432" 
    environment:
      # Deafult database configuration, It creates the default biot database
      - POSTGRES_DB=biot 
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres 

  minio:
    image: minio/minio
    hostname: minio
    volumes:
      - './prod/minio/data/:/data' 
      - './prod/minio/config:/root/.minio' 
    environment:
      - MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLF # Minio access key / To be defined for the admin
      - MINIO_SECRET_KEY=wJalrXUtnFEMIK7MDENGbPxRfiCYEXAMPLEKEW # Minio secret / To be defined for the admin
    command: server /data

  openmaptiles:
    image: klokantech/tileserver-gl
    volumes:
      - './prod/tileserver/data:/data'

  geoserver:
    image: coderobotmx/geoserver:latest
    volumes:
      - ./prod/geoserver/web.xml:/usr/local/tomcat/conf/web.xml
      - ./prod/geoserver/data:/opt/geoserver/data_dir
      - ./prod/geoserver/lib/gs-querylayer-2.14.2.jar:/opt/geoserver/webapps/geoserver/WEB-INF/lib/gs-querylayer-2.14.2.jar
    depends_on:
      - postgres

  arboliot:
    image: registry.gitlab.com/coderobot-giz/biot-2.0
    environment:
      - SPRING_PROFILES_ACTIVE=prod,swagger
      - SERVER_USE_FORWARD_HEADERS=true
      # Database configuration / same credentials and container name as defined in the postgres database container
      - SPRING_DATASOURCE_URL=jdbc:postgresql://postgres:5432/biot
      - SPRING_DATASOURCE_USERNAME=postgres
      - SPRING_DATASOURCE_PASSWORD=postgres
      # S3 Service configuration / same credentialds and container name as defined in the minio container
      - APPLICATION_MIN_IO_END_POINT=http://minio:9000
      - APPLICATION_MIN_IO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLF
      - APPLICATION_MIN_IO_SECRET_KEY=wJalrXUtnFEMIK7MDENGbPxRfiCYEXAMPLEKEW
      # Access tokens for sigfox / To be defined for the admin, this key allows to protect the /api/devices/web-hook endpoint
      - APPLICATION_SIG_FOX_TOKEN_0=secret-to-config-as-bearer-in-sigfox 
      # General configuration / replace all domain ocurrences with your own domain
      - APPLICATION_BASE_URL=https://domain
      - APPLICATION_CLIENT_MAP_CENTER=POINT(-103.3932983 20.7244222)
      - SPRING_HTTP_MULTIPART_MAX-FILE-SIZE=100MB
      - SPRING_HTTP_MULTIPART_MAX-REQUEST-SIZE=100MB
      - JHIPSTER_SECURITY_AUTHENTICATION_JWT_SECRET=my-secret-token # To be defined for the admin, it allows to generate user tokens
      - JHIPSTER_SLEEP=10 
      - JHIPSTER_LOGGING_LOGSTASH_ENABLED=false
      - JHIPSTER_LOGGING_LOGSTASH_HOST=logstash
      - JHIPSTER_METRICS_LOGS_ENABLED=false
      - JHIPSTER_METRICS_PROMETHEUS_ENABLED=false
      - JHIPSTER_MAIL_FROM=mail@mail.com
      - JHIPSTER_MAIL_BASE_URL=https://domain
      - APPLICATION_CLIENT_MAP_BASE_LAYER=https://domain/styles/dark-matter 
      - APPLICATION_CLIENT_MAP_URL=https://domain/geoserver
      - APPLICATION_CLIENT_MAP_CENTER=POINT(-103.3704324 20.6737883)
      - APPLICATION_CLIENT_MAP_MIN_ZOOM=10
      - APPLICATION_CLIENT_MAP_MAX_ZOOM=18
      - APPLICATION_CLIENT_MAP_ZOOM=12
      - APPLICATION_CLIENT_MAP_WORKSPACE=arbol-iot # Warning, it data is not customizable, it is only a reference for new layers
      - APPLICATION_CLIENT_MAP_LAYERS_INTERNAL_0_LABEL=Árboles  # Warning, it data is not customizable, it is only a reference for new layers 
      - APPLICATION_CLIENT_MAP_LAYERS_INTERNAL_0_PARAMS_LAYERS=arbol-iot:tree  # Warning, it data is not customizable, it is only a reference for new layers
      - APPLICATION_CLIENT_MAP_LAYERS_INTERNAL_0_PARAMS_TILED=true  # Warning, it data is not customizable, it is only a reference for new layerse 
      - APPLICATION_DEFAULT_BOUNDARY_ID=1024914 # Default boundary for user creation
      - APPLICATION_OTM_REGION=PiedmtCLT  # Default OTM Region
      - APPLICATION_SOUTHERN=false # Temporal and Weather station type
      # Google Social Login credentials 
      # Obtain in https://developers.google.com/+/web/signin/server-side-flow#step_1_create_a_client_id_and_client_secret
      - SPRING_SOCIAL_GOOGLE_CLIENT_ID=
      - SPRING_SOCIAL_GOOGLE_CLIENT_SECRET=
      # Obtains in
      - APPLICATION_CLIENT_GOOGLE_WEB_CLIENT_ID=
      - APPLICATION_CLIENT_GOOGLE_IOS_CLIENT_ID=
      # Facebook Social Login credentials 
      # Obtain in https://developers.facebook.com/docs/facebook-login/v2.2
      - SPRING_SOCIAL_FACEBOOK_CLIENT_ID=
      - SPRING_SOCIAL_FACEBOOK_CLIENT_SECRET=
      # Google Firebase Cloud Messaging credentials
      # Obtain in https://console.firebase.google.com/u/0/?pli=1
      - APPLICATION_FIREBASE_APP_ID=# example-account
      - APPLICATION_FIREBASE_ACCOUNT_ID=# example-account@@appspot.gserviceaccount.com
      - APPLICATION_FIREBASE_PRIVATE_KEY=# "-----BEGIN PRIVATE KEY...."
      # Open Weather Map Service credentials
      # Obtain in https://openweathermap.org/api
      - APPLICATION_OPEN_WEATHER_MAP_ENABLED=true
      - APPLICATION_OPEN_WEATHER_MAP_TOKEN=
      - APPLICATION_OPEN_WEATHER_MAP_CITY=Guadalajara
      - APPLICATION_OPEN_WEATHER_MAP_COUNTRY=MX
      - APPLICATION_OPEN_WEATHER_MAP_LANGUAGE=sp
      - APPLICATION_OPEN_WEATHER_MAP_UNIT=metric
      # AQICN Service credentials
      # Obtain in http://aqicn.org API
      - APPLICATION_AQI_CN_ENABLED=true
      - APPLICATION_AQI_CN_TOKEN= # secret-token
      - APPLICATION_AQI_CN_BOUNDARY=20.5994625,-103.2974338,20.7407657,-103.3830465 # Obtain all devices in this area
      # SMTP Mail configuration 
      # Obtain with your smtp provider
      - SPRING_MAIL_HOST=smtp.gmail.com
      - SPRING_MAIL_PORT=587
      - SPRING_MAIL_USERNAME=
      - SPRING_MAIL_PASSWORD=
      - SPRING_MAIL_PROTOCOL=
      - SPRING_MAIL_TLS=true
      - SPRING_MAIL_PROPERTIES_MAIL_SMTP_AUTH=true
      - SPRING_MAIL_PROPERTIES_MAIL_SMTP_STARTTLS_ENABLE=true
      - SPRING_MAIL_PROPERTIES_MAIL_SMTP_SSL_TRUST=smtp.gmail.com
    depends_on:
      - postgres
      - minio
```

A continuación se listan las variables de entorno requeridas para la instalación inicial, las variables no descritas son secundarias
y se auto describen en el documento YML.

#### Contenedor Postgres / Entorno

Se recomienda mantener el nombre de la base de datos y cambiar la contraseña default, para esto cambiar los siguientes atributos:

```
    - POSTGRES_USER=youruser
    - POSTGRES_PASSWORD=yourpassword 
```
#### Contenedor MinIo / Entorno

Es necesario generar una clave de acceso y una clave secreta para activar la seguridad de este contenedor, utilizar cadenas de tipo MD5

```
    - MINIO_ACCESS_KEY=youraccesskey 
    - MINIO_SECRET_KEY=yoursecretkey 
```
#### Contenedor Árbol IoT / Entorno / Base de datos

Actualizar credenciales de conexión a la base de datos BIOT, considerar que estas deben ser las mismas que las definidas en el apartado 'Contenedor Postgres / Entorno'

```
    - SPRING_DATASOURCE_USERNAME=youruser
    - SPRING_DATASOURCE_PASSWORD=yourpassword
```
#### Contenedor Árbol IoT / Entorno / MinIo

Actualizar credenciales de conexión a Contenedor MinIo, considerar que estas deben ser las mismas que las definidas en el apartado 'Contenedor MinIo / Entorno'

```
    - APPLICATION_MIN_IO_ACCESS_KEY=youraccesskey
    - APPLICATION_MIN_IO_SECRET_KEY=yoursecretkey
```
#### Contenedor Árbol IoT / Entorno / Sigfox

Este apartado permite agregar claves de acceso al endpoint '/api/devices/web-hook', se requiere agregar al menos un token, utilizar cadenas de tipo MD5, estas serán utilizadas
una vez se configure el endpoint de sigfox más adelante.

```
    - APPLICATION_SIG_FOX_TOKEN_0=secret-to-config-as-bearer-in-sigfox 
```
#### Contenedor Árbol IoT / Entorno / Dominio

Remplazar las referencias a domain por el dominio público a utilizar

```
      - APPLICATION_BASE_URL=https://domain
      - APPLICATION_CLIENT_MAP_URL=https://domain/geoserver
      - JHIPSTER_MAIL_BASE_URL=https://domain
      - APPLICATION_CLIENT_MAP_BASE_LAYER=https://domain/styles/dark-matter 
      - APPLICATION_CLIENT_MAP_URL=https://domain/geoserver
``` 

Configurar también el correo de salida

```
      - JHIPSTER_MAIL_FROM=mail@mail.com
```
#### Contenedor Árbol IoT / Entorno / Seguridad

Asignar cadena MD5 utilizada para la generación de tokens JWT

```
    - JHIPSTER_SECURITY_AUTHENTICATION_JWT_SECRET=my-secret-token
```
#### Contenedor Árbol IoT / Entorno / Región OTM - iTREE

El módulo de eco-beneficios está basado en el eco-service de Open Tree Map / iTree Streets el cual cuenta con un modelo basado en regiones climáticas,
hay 16 regiones climáticas disponibles, seleccionar la que más se adapte al entorno local

- Temperate Interior West / TpIntWBOI
- Northeast / NoEastXXX
- Northern California Coast / CaNCCoJBK
- Interior West / InterWABQ
- Inland Empire / InlEmpCLM
- Lower Midwest / LoMidWXXX
- Midwest / MidWstMSP
- North / NMtnPrFNL
- Pacific Northwest / PacfNWLOG
- South / PiedmtCLT
- Southern California Coast / SoCalCSMA
- Coastal Plain / GulfCoCHS
- Southwest Desert / SWDsrtGDL
- Inland Valleys/ InlValMOD
- Central Florida / CenFlaXXX
- Tropical / TropicPacXXX

```
      - APPLICATION_OTM_REGION=PiedmtCLT  # Default OTM Region
```
#### Contenedor Árbol IoT / Entorno / Inicio de sesión a través de Google

Primero, obtener las credenciales necesarias para el uso de Google Social Login desde el componente web,
para esto crear aplicación OAuth Web desde el siguiente enlace:

[https://developers.google.com/+/web/signin/server-side-flow#step_1_create_a_client_id_and_client_secret](https://developers.google.com/+/web/signin/server-side-flow#step_1_create_a_client_id_and_client_secret)

Una vez creada se obtendrán los siguientes metadatos:

![Screenshot](../images/img-4.png)

- a) __Id de cliente__: Utilizar su valor para la variable SPRING_SOCIAL_GOOGLE_CLIENT_ID y para la variable APPLICATION_CLIENT_GOOGLE_WEB_CLIENT_ID
- b) __Clave secreta__: utilizar su valor para la variable SPRING_SOCIAL_GOOGLE_CLIENT_SECRET

```
      - SPRING_SOCIAL_GOOGLE_CLIENT_ID= 
      - SPRING_SOCIAL_GOOGLE_CLIENT_SECRET=
      - APPLICATION_CLIENT_GOOGLE_WEB_CLIENT_ID=
```

Adicionalmente, es necesario obtener un CLIENT_ID específico para iOS, crear un nuevo cliente OAuth siguiendo los pasos anteriores
pero seleccionando como tipo de aplicación 'Apliación iOS' tal cual se visualiza a continuación:

![Screenshot](../images/img-5.png)

- a) Considerar que el bundle-id definido en este apartado deberá corresponder con el bundle-id de la aplicación iOS a publicar.

Una vez creada la aplicación, se obtendrá el APPLICATION_CLIENT_GOOGLE_IOS_CLIENT_ID como se visualiza a continuación:

![Screenshot](../images/img-6.png)

```
      - APPLICATION_CLIENT_GOOGLE_IOS_CLIENT_ID= 
```
#### Contenedor Árbol IoT / Entorno / Inicio de sesión a través de Facebook

Obtener las credenciales de inicio de sesión de Facebook como se indica en el siguiente enlace:

[https://developers.facebook.com/docs/facebook-login/web/](https://developers.facebook.com/docs/facebook-login/web/)

Una vez creada la aplicación obtendrás los siguientes metadatos:

- a) SPRING_SOCIAL_FACEBOOK_CLIENT_ID
- b) SPRING_SOCIAL_FACEBOOK_CLIENT_SECRET

![Screenshot](../images/img-7.1.png)

Mismos que se tendrán que asignar en las siguientes variables:

```
      - SPRING_SOCIAL_FACEBOOK_CLIENT_ID= 
      - SPRING_SOCIAL_FACEBOOK_CLIENT_SECRET=
```
#### Contenedor Árbol IoT / Entorno /  Firebase Cloud Messaging

Árbol IoT utiliza Google Firebase Cloud Messaging para el envío de notificaciones push, para obtener las API Key de acceso es necesario 
crear un proyecto de firebase desde el siguiente enlace:

[https://console.firebase.google.com/u/0/?pli=1](https://console.firebase.google.com/u/0/?pli=1)

Una vez creado seguir los siguientes pasos:

![Screenshot](../images/img-7.png)

1. Acceder al apartado de configuración del proyecto
2. Acceder al apartado de cuentas de servicio
3. Acceder al apartado de otras cuentas de servicio

En el apartado otras cuentas de servicio encontraras la siguiente distribución de contenido:

![Screenshot](../images/img-8.png)

1. Prestar atención a la cuenta con dominio @appspot.gserviceaccount.com 
2. Acceder a la opción 'Create key'

![Screenshot](../images/img-9.png)

1. Seleccionar la opción de crear una nueva clave de tipo JSON

Este último paso generará un archivo de tipo JSON, abrir en cualquier editor de texto y recuperar de este las variables de configuración 

![Screenshot](../images/img-10.png)

Asignar los atributos como se detalla a continuación:

1. El valor PROJECT_ID para la variable de entorno APPLICATION_FIREBASE_APP_ID (a)
2. El valor CLIENT_EMAIL en la variable de entorno APPLICATION_FIREBASE_ACCOUNT_ID (b)
3. El valor PRIVATE_KEY en la variable de entorno APPLICATION_FIREBASE_PRIVATE_KEY (c)

```
      - APPLICATION_FIREBASE_APP_ID=
      - APPLICATION_FIREBASE_ACCOUNT_ID=
      - APPLICATION_FIREBASE_PRIVATE_KEY= 
```
#### Contenedor Árbol IoT / Entorno / Open Weather Map

Árbol IoT obtiene los datos de temperatura y de humedad de un servicio de terceros llamado Open Weather Map, 
para utilizar este servicio es necesario obtener una API KEY como se detalla a continuación:

[https://openweathermap.org/appid](https://openweathermap.org/appid)

```
      - APPLICATION_OPEN_WEATHER_MAP_TOKEN= 
```
#### Contenedor Árbol IoT / Entorno / aqicn.org

Árbol IoT obtiene datos validados de calidad el aire de aqicn.org, para utilizar este servicio es necesario obtener una API KEY como se detalla a continuación:

[https://aqicn.org/data-platform/token/#/](https://aqicn.org/data-platform/token/#/)

```
      - APPLICATION_AQI_CN_TOKEN=
```
#### Contenedor Geoserver 

Antes de iniciar Árbol IoT por primera vez es necesario actualizar las credenciales de este servicio, proceder como se detalla a continuación:

&nbsp;

###### Actualizar contraseña general
```
nano ./prod/geoserver/data/security/usergroup/default/users.xml
```
![Screenshot](../images/img-11.png)

###### Actualizar credenciales de conexión a la base de datos BIOT

```
nano ./prod/geoserver/data/workspaces/arbol-iot/arbol-iot/datastore.xml
```
![Screenshot](../images/img-12.png)

* Considerar que estas deben ser las mismas que las definidas en el apartado 'Contenedor Postgres / Entorno'

### Inicio de contenedores

Iniciar sesión con tus credenciales de gitlab en el docker registry para obtener la imagen de árbol iot:
```
docker login registry.gitlab.com
```

Iniciar los contenedores asociados a la solución:
```
cd /var/lib/aiot
docker-compose up -d
```
Detener los contenedores asociados a la solución:
```
docker-compose down
```
Validar contenedores iniciados:
```
docker ps
```
![Screenshot](../images/img-12.1.png)

* Deberán desplegarse al menos 6 contenedores con servicios relacionados a la solución

* Se puede acceder a los logs de cada contenedor a través del comando 'docker logs -f [id-contenedor]' 
&nbsp;
### Acceder a aplicación web Árbol IoT

Para acceder a la solución, ingresar al dominio configurado a través del puerto https como se indica a continuación:

* https://domain

Para iniciar sesión utilizar las credenciales por defecto:

* user:admin 
* password:password

### Actualización de datos generales de instancia

Para configurar los datos generales de instancia, iniciar sesión con las cuentas default y acceder al apartado de configuración avanzada como se detalla
a continuación:

![Screenshot](../images/img-12.2.png)

![Screenshot](../images/img-12.3.png)

En este apartado se podrá actualizar la configuración de instancia, la cual está en formato JSON como se detalla a continuación:

```
{ 
"scales":{
    "temp": "℃",
    "humidity": "%"
},
"login": {
  "facebook":false,
  "google": false
},    
"instanceName":"Ciudad verde",
"boundaryModule": {
    "defaultBoundary":214
},
"treeModule": {
    "buffer": 5    
},
"commonSpecies": [],
"sources": {
    "temp": " Los datos de temperatura y humedad se basan en promedio por ciudad y son proporcionados por openweathermap.org",
    "aq": "Los datos de calidad del aire estan basados en la localización del usuario / estación cercana y son proporcionados por aqicn.org"
}, 
"reportModule":{
    "enabled": false,
    "message": "Marca al 072 para levantar un reporte a través de la línea de atención ciudadana",
    "phone": "072"
},
"communityModule": 
    {
        "closeReasons": [
            "INAPPROPRIATE_CONTENT",
            "TOPIC_RESOLVED",
            "INACTIVITY",
            "OTHER"
            ] 
    },
"treeCommentsModule": 
    {
        "closeReasons": [
            "INCORRECT_SPECIE",
            "INCORRECT_MESAURES",
            "INCORRECT_PHOTO",
            "TREE_NOT_FOUND",
            "OTHER"
            ] 
    },    
"userPolicyModule": 
    { 
        "policyDownReasons": [ 
            "CREATION_OF_FALSE_CONTENT",
            "INACTIVITY",
            "OTHER"
            ],
        "policyNewAtributions": [ "Ascender usuarios", "Validación de árboles", "Creación de trivias", "Moderador en la sección de comunidad" ]
    }, 
"userRegister": 
    { 
        "showOnlyTermsAndConditions":true,
        "termsAndConditions": "Por definir",  
        "privacyPolicy": "Por definir"
    }, 
"userTrees": 
    { 
        "seasonSupportDays": 11
    },
"policiesByRole": 
    {
        "ROLE_USER": {
            "treeValidation": {
                "view": { "status":false, "desc":"" }
            },
            "userPolicyUp": {
                "view": { "status":false, "desc":"" }
            },
            "community": {
                "toggleSticky": { "status":false, "desc":"" },
                "toggleClose": { "status":false, "desc":"" }
            }     
        },
        "ROLE_USER_PRO": {
            "treeValidation": {
                "view": { "status":true, "desc":"" }
            },
            "userPolicyUp": {
                "view": { "status":true, "desc":"" }
            },
            "community": {
                "toggleSticky": { "status":true, "desc":"" },
                "toggleClose": { "status":true, "desc":"" }
            }     
        }
    }
}
```
&nbsp;
### Sincronización automática de mediciones de calidad del aire

Una vez iniciada la aplicación, esta sincronizará los sensores disponibles de aqicn.org basado en los límites geográficos definidos en la siguiente variable:

```
  - APPLICATION_AQI_CN_BOUNDARY=20.5994625,-103.2974338,20.7407657,-103.383046
```

Para habilitar la sincronización de estos dispositivos, realizar los siguientes pasos:

![Screenshot](../images/img-12.4.png)

![Screenshot](../images/img-12.5.png)

![Screenshot](../images/img-12.6.png)


* Es necesario hablitar al menos un dispositivo de  aqicn.org ya que la aplicación móvil extrae información de este servicio
* Las mediciones se sincronizan cada 15 minutos por diseño

## Árbol IoT Móvil

Para compilar y publicar la aplicación iOS y la aplicación Android se requiere de un equipo OSX con xCode, Android Studio y brew instalado, una vez 
satisfechas las dependencias anteriores proceder como se detalla a continuación:

### Preparación de entorno

#### React Native:

Instalar entorno de React Native

```
brew install yarn
brew install node
brew install watchman
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
```
[https://facebook.github.io/react-native/docs/getting-started](https://facebook.github.io/react-native/docs/getting-started)
&nbsp;
#### React Native CLI / React Native Rename

Instalar cliente React Native y utilidad de renombrado de aplicaciones
```
npm install -g react-native-cli
npm install -g react-native-rename 
```

#### Código fuente

Obtener código fuente del repositorio:

```
git clone https://gitlab.com/coderobot-giz/arbol-iot.git
```
#### Obtención de dependencias y librerías relacionadas
```
cd arbol-iot
npm install
```
#### Actualización de nombre de paquete

Cuando se realice la publicación de la aplicación en las respectivas tiendas, será necesario contar con un nombre único de paquete, actualizar el mismo
recordando que este deberá coincidir con el asignado en el apartado 'Contenedor Árbol IoT / Entorno / Inicio de sesión a través de Google / Bundle id'

```
react-native-rename "ArbolIot" -b com.test.arboliot
```

#### Actualización de endpoint

Actualizar la constante 'base' en el archivo  'src/config/constants.js' del código fuente, esta debera coincidir con el nombre de dominio de la aplicación web:

```
nano src/config/constants.js
```

![Screenshot](../images/img-13.png)

#### Configurar API key de Google Maps / Android / iOS

Obtener API key de Google a través de la siguiente guía: [https://developers.google.com/maps/documentation/android-sdk/get-api-key](https://developers.google.com/maps/documentation/android-sdk/get-api-key)

Remplazar la key obtenida en el archivo AndroidManifest.xml como se detalla a continuación:

```
nano android/app/src/main/AndroidManifest.xml
```
![Screenshot](../images/img-14.png)


Y en el archivo AppDelegate.m como se detalla a continuación:

```
nano ios/ArbolIoT/AppDelegate.m
```
![Screenshot](../images/img-14.0.png)

#### Configurar Facebook Social Login / Android / iOS

Apoyarse de la documentación oficial de Facebook disponible en el siguiente enlace para Android:
[https://developers.facebook.com/docs/facebook-login/android](https://developers.facebook.com/docs/facebook-login/android)

* Considerar solamente los pasos 4, 5, 6 y 7 de la documentación, recordando reutilizar la aplicación creada en el apartado 'Contenedor Árbol IoT / Entorno / Inicio de sesión a través de Facebook'.


Y en el siguiente enlace para iOS:
[https://developers.facebook.com/docs/facebook-login/ios](https://developers.facebook.com/docs/facebook-login/ios)

* Considerar solamente los pasos 3, 4 de la documentación, recordando reutilizar la aplicación creada en el apartado 'Contenedor Árbol IoT / Entorno / Inicio de sesión a través de Facebook'.


#### Configurar Firebase / Android / iOS

A partir de la aplicación Firebase definida en el apartado 'Contenedor Árbol IoT / Entorno / Firebase Cloud Messaging', ingresar a la misma y
agregar una nueva aplicación Android como se muestra a continuación:

![Screenshot](../images/img-14.1.png)

Configurar el nombre de paquete y la información de manera consecuente a las configuraciones anteriores:

![Screenshot](../images/img-14.1.1.png)

Descargar el archivo de configuración google-services.json y almacenarlo en el siguiente directorio:

![Screenshot](../images/img-14.1.2.png)

```
android/app/google-services.json
```

* Una vez obtenido este archivo no es necesario continuar con los pasos siguientes dentro del asistente de Firebase

Para la versión iOS, agregar una nueva aplicación iOS como se muestra a continuación:

![Screenshot](../images/img-14.2.png)

Configurar el nombre de paquete y la información de manera consecuente a las configuraciones anteriores:

![Screenshot](../images/img-14.2.1.png)

Descargar el archivo de configuración GoogleService-Info.plist y almacenarlo en el siguiente directorio:

![Screenshot](../images/img-14.2.2.png)

```
ios/ArbolIoT/GoogleService-Info.plist
```

#### Actualizar firmas de aplicación / Android

Apoyarse de la documentación oficial de Google disponible en el siguiente enlace:
[https://developer.android.com/studio/publish/app-signing](https://developer.android.com/studio/publish/app-signing)


### Ejecutar la aplicación / Publicar

Para ejecutar la aplicación en modo local o publicar la misma, referirse a la documentación oficial de React Native:

[https://facebook.github.io/react-native/docs/running-on-device](https://facebook.github.io/react-native/docs/running-on-device)











