# Módulos y roles

En este apartado se describe a detalle a cada uno de los roles y módulos incluidos en la solución tecnológica Árbol IoT.

## Roles

La solución contempla 4 roles escenciales, mismos que se describen a continuación:

### Usuario 

Rol asignado inicialmente a todas las personas que se registran a través de la aplicación móvil, estos usuarios pueden acceder a los módulos y funciones que su nivel de experiencia y puntaje les permita, dichas consideraciones están definidas en la tabla de niveles y puntos del apartado de gamificación.

### Usuario pro

Este rol permite a los usuarios PRO ya sean moderadores o expertos realizar tareas administrativas de moderación de comunidad y validación del arbolado urbano a través de la aplicación móvil, para tener este rol tuvieron que ser ascendidos previamente por un administrador.

### Administrador de contenido

Este rol está pensado para ser asignado a los actores administrativos y profesionales dentro de la institución gubernamental u organización encargada de mantener la plataforma operativa y de asegurar la calidad y veracidad de la información vertida en ella. Se considera a profesionales medioambientales y moderadores mismos que serán encargados de mantener a la comunidad y realizar la  validación del arbolado urbano. Este tipo de usuario también es el encargado de realizar  el ascenso de los primeros usuarios pro.

### Administrador general

Este rol está pensado para ser asignado a los administradores técnicos o generales de la solución, es el rol con más privilegios y nivel de acceso, permitiendo así administrar de manera transversal a todos los módulos incluidos en la solución.

## Módulos

La solución se estructura a manera de módulos, siendo estos accesibles desde la aplicación móvil o web, a continuación se describen los módulos y características principlaes incluidas en la plataforma:

### Gestión de cuenta

La aplicación Árbol IoT es una solución que se adapta a los usuarios y muestra información personalizada según el comportamiento del mismo, por tal motivo requiere de una cuenta de usuario, este módulo tiene la finalidad de proveer las funcionalidades necesarias para que un usuario pueda obtener y administrar una cuenta.

### Tutorial
Este módulo facilita el uso de la aplicación móvil a través de una guía dinámica que provee información relacionada a las funciones accesibles por el usuario.

### Dashboard principal
Este módulo proporciona información dinámica ambiental y acceso directo a las funcionalidades más importantes dentro de la app.

### Gestión de arbolado urbano
Este módulo facilita la visualización, captura y edición del arbolado urbano a través de un mapa de exploración.

### Validación de arbolado urbano
Este módulo permite realizar la validación y corrección de los datos del arbolado urbano.

### Ascender usuario
Este módulo permite efectuar el ascenso de un usuario a usuario pro.

### Perfil de usuario
Este módulo permite mostrar la información relativa al usuario, sus logros y compromisos, así como las funciones que permiten al mismo conectar con otros usuarios.

### Reporte de contribución ambiental del usuario (Mi contribución)
Este apartado despliega un reporte infográfico de la contribución del usuario al medio ambiente, este se calcula a partir de las acciones e interacciones realizadas dentro de la app.

### Reporte de beneficios ecológicos
Este apartado despliega un reporte infográfico de los beneficios ecológicos del arbolado urbano para un contexto geográfico específico.

### Mis árboles  (Árboles adoptados)
Este apartado incluye a las funciones relacionadas con la adopción de árboles. Cada árbol adoptado se transforma en una árbol digital el cual guarda una estrecha relación con el árbol físico, requiriendo así, cuidados, actualizaciones de estado y fotos para el mantenimiento del mismo

### Comunidad
Este módulo tiene la finalidad de crear comunidad entre usuarios de la plataforma, con el objetivo de promover el aprendizaje y la participación de los mismos.

### Trivias
Este apartado tiene la finalidad de fomentar la educación digital, la investigación y la adquisición de nuevos conocimiento a través de preguntas desplegadas a manera de trivia.

### Gestión de reportes
Este módulo facilita la visualización, captura y seguimiento de reportes

### Notificaciones
Este módulo tiene la finalidad de desplegar las notificaciones relacionadas a las acciones realizadas sobre la aplicación.

