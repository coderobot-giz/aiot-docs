# Guía de usuario pro
Un usuario PRO, es aquel usuario que a través de los méritos logrados por sus aportaciones en la aplicación móvil es considerado por los usuarios administradores o managers como apto para este rol motivo por el cual lo han promovido.

Antes de entrar en materia acerca de las facultades a las que tiene acceso un usuario con este rol, se considera pertinente describir la distribución de componentes en la plataforma para facilitar la localización de módulos y funcionalidades.

## Distribución general
Durante la navegación realizada dentro de la plataforma se distinguen cinco regiones principales:

1.  __Barra de navegación principal:__ Incluye un acceso directo a al página principal, así como las opciones de inicio de sesión, o el menú de usuario (después de haber iniciado sesión).
2.  __Braniding:__ Hace referencia a las organizaciones que impulsan el proyecto.
3.  __Menú secundario:__ Vínculos a los contenidos relevantes del sitio y que son de acceso público.
4.  __Contenido:__ Espacio donde se muestra la información o contenido principal de la página visitada.
5.  __Pie de página:__ Información referente a la construcción de la plataforma.

![Screenshot](../images/img-m-1.png)

Habiendo comprendido la distribución de la plataforma, a continuación se comenzará a describir el proceso de acceso a la misma, así como las acciones que puede realizar sobre esta.

## Inicio de sesión

Se puede acceder al formulario de inicio de sesión mediante dos métodos: 

1. Localizando la opción *"Iniciar sesión"* localizada en la barra de navegación principal, en la parte superior derecha de la pantalla

2. Al ingresar a la ruta principal del sitio agregando a la url  *"/login"*

Una vez ingresados los datos de acceso, e iniciada la sesión, se __visualizará__ información adicional en la barra de navegación, mostrando el nombre, el apellido y la imagen de perfil del usuario en caso de estar disponible, cualquiera de estos elementos permitirán desplegar el *"menú de usuario"*, y en caso de que el usuario tenga permisos para ello, se mostrará el acceso a otros apartados de la plataforma.

![Screenshot](../images/img-m-3.png)

Un usuario logueado en la plataforma, puede tener alguno de los siguientes roles:

* Usuario / Usuario
* Usuario pro / Usuario pro
* Manager / Administrador de contenido
* Admin / Administrador general

A continuación se detallará cuáles son las actividades que puede realizar un usuario con rol de usuario PRO.

## Menú de usuario
Este se desplegara al hacer click sobre la flecha localizada en el nombre de usuario o foto de perfil y mostrará las áreas adicionales a las cuales tendremos acceso.

![Screenshot](../images/img-m-15.png)

### Mi cuenta
En esta sección se muestra la información capturada al crear la cuenta y algunos datos adicionales que permiten mejorar la experiencia en el uso de la aplicación móvil Árbol IoT, esta interface tiene dos comportamientos:

1) Al solo ser usuarios de la plataforma.

![Screenshot](../images/img-m-5.png)

2) Al ser usuario de la plataforma y ser usuario activo en la aplicación móvil.

![Screenshot](../images/img-m-6.png)

* A través de la opción __Editar__ de esta interface se permite desplegar la interface de edición de cuenta.

### <a name="validar"></a>Validar árboles
Mediante el uso de la aplicación móvil Árbol IoT, los usuarios pueden capturar o modificar la información referente al arbolado urbano, los usuarios con este rol podrán determinar si la información registrada es válida o no, así como realizar las modificaciones pertinentes. El flujo de este módulo se detalla a continuación:

* Listar árboles por validar

![Screenshot](../images/img-m-16.png)

* Seleccionar opción de validación y consulta de ficha del árbol

![Screenshot](../images/img-m-17.png)

* Modificación de estatus de ficha. 

![Screenshot](../images/img-m-18.png)

Existen tres estatus posibles para la ficha de árbol:

- __Incompleto__: Permite que el árbol pueda seguir siendo editado por cualquier usuario
- __Por validar__: Estatus asignado por un usuario a través de la aplicación móvil para solicitar la validación del mismo, este estatus bloquea futuras ediciones por parte de otros usuarios
- __Validado__: Estatus asignado por un usuario PRO o Manager una vez acreditan la validez de los datos registrados. Una vez en este estatus solo los usuarios con rango manager o superior podrán volver a __Editarlo__ en caso de ser necesario

### Crear trivia
Una trivia es un conjunto de preguntas que tienen la finalidad de alimentar al módulo de trivias de la aplicación móvil, cada una de ellas es de opción múltiple y se crean como se detalla a continuación:

1.&nbsp;Leer los requisitos previos a la creación de una trivia y seleccionar continuar

![Screenshot](../images/img-m-19.png)

2.&nbsp;Capturar el conjunto de preguntas requeridas siguiendo las instrucciones del formulario de captura

![Screenshot](../images/img-m-20.png)

Podrás navegar entre las preguntas creadas para modificar algún dato en caso de ser necesario

![Screenshot](../images/img-m-21.png)

### Ascender usuarios

En este apartado se lista a los usuarios que interactúan en la aplicación móvil Árbol IoT, identificando dos áreas principales:

![Screenshot](../images/img-m-22.png)

1. __Área de filtrado__: Permite decidir si queremos ver a los usuarios que ya fueron promovidos a usuario PRO o consultar a los posibles candidatos
2. __Listado de usuarios__: Listado de usuarios con botón de acción para ascender a los mismos

Al presionar el botón desde el área el listado de candidatos se desplegará una alerta donde se deberá aceptar que se le asignarán nuevas atribuciones al usuario en cuestión


![Screenshot](../images/img-m-23.png)

Al presionar el botón desde el listado de usuarios PRO, se podrá remover las atribuciones de usuario convirtiéndolo en un usuario normal


![Screenshot](../images/img-m-24.png)

Al degradar a un usuario se deberá especificar el motivo por el cual se ha realizando la acción


![Screenshot](../images/img-m-25.png)


### Contraseña

Este apartado permite realizar la actualización de la contraseña de acceso a la plataforma, para ello solo debemos capturar los campos solicitados y guardar los ajustes.

### Cerrar sesión
Este apartado permite finalizar la sesión activa. Al finalizar la sesión, automáticamente se redireccionará a la interface principal del sitio.

* Con esto finalizamos de describir las áreas a las cuales se tiene acceso a través del menú de usuario.

## Visor del arbolado
Este apartado se localiza en la página principal del sitio, podemos acceder a este módulo a través del menú secundario o mediante el icono "Home" localizado en la barra de navegación principal.

En este apartado se tendrá acceso y herramientas para interactuar con el arbolado urbano.

Esta interface se conforma de las siguientes regiones principales:

![Screenshot](../images/img-m-27.png)

1. __Mapa:__ En este apartado podremos observar los puntos del arbolado urbano basados en la colorimetría descrita en el punto 3. Al hacer click sobre un punto podremos acceder a sus detalles en el área de *información descriptiva*
2. __Zoom control:__ Permite acercar o alejar la vista del mapa, este control también se puede realizar mediante el scroll del mouse
3. __Leyenda:__ Información que describe la colorimetría utilizada para la representación de datos en el mapa
4. __Buscador:__ Permite delimitar la información geográfica mostrada en el mapa a través de búsquedas por colonias o polígonos, también podemos realizar búsquedas sobre la información del arbolado urbano en este caso por especie.
5. __Configuración y operaciones:__ Configuraciones avanzadas para el manejo de los datos a mostrar en el mapa, el usuario con este rol puede realizar las siguientes configuraciones.
    - __Agregar árbol__: Brinda acceso al formulario de captura en el cual se solicitará información como la especie del árbol, el diámetro del tronco, el diámetro de la copa, la altura, la georeferencia, el estatus de la información y una foto del árbol.
    ![Screenshot](../images/img-m-28.png)
    - __Capas__: Configurar la capa base del mapa, así como las capas de datos disponibles.
    ![Screenshot](../images/img-m-9.png)
    - __Filtros__: Filtros sobre la información del arbolado urbano.
    ![Screenshot](../images/img-m-10.png)
    - __Localizar__: Permite centrar el mapa de acuerdo a la georeferencia actual del dispositivo utilizado.
    - __Exportar__: Permite descargar un archivo de datos con la información del arbolado urbano, esta descarga responde a los filtros especificados en el visor. 
6. __Información descriptiva:__ Este apartado muestra los beneficios ecológicos que brinda el arbolado urbano, estos beneficios están condicionados a la información contextual y/o de búsqueda. A su vez al interactuar con el mapa nos mostrará la información del árbol en seleccionado y un enlace a la ficha detallada del mismo.
Al interactuar con el botón ver ficha nos enviará a una pantalla como la siguiente; 
    ![Screenshot](../images/img-m-26.png)
    En este ejemplo se pueden ver los datos de la ficha del árbol, compartir en redes sociales y de considerarlo necesario __Editar__ la información del mismo.

## Comunidad
Puedes acceder a este apartado desde el menú secundario, al acceder podemos __Visualizar__ dos secciones:

![Screenshot](../images/img-m-29.png)

1. __Acciones:__  Permite realizar filtrados textuales, por etiquetas o por indicador de publicaciones destacadas.
2. __Listado de publicaciones:__  Permite desplegar el listado de publicaciones, ordenadas por fecha de publicación. 

A diferencia del usuario normal, el usuario PRO puede tener mayor interacción con la sección *Comunidad*, desde este apartado puede realizar las siguientes actividades:
![Screenshot](../images/img-m-31.png)

1. Crear una publicación en la comunidad, al entrar a la sección de Comunidad y posteriormente hacer click en el icono de captura localizado en el área de *acciones*, esto dará acceso al formulario de captura.
![Screenshot](../images/img-m-30.png)
2. El usuario puede dar me gusta a la publicación.
3. El usuario puede acceder a la ficha de la publicación al hacer click en el icono de comentario.
4. En la ficha de la publicación, el usuario podrá dejar comentarios al respecto.
En caso de existir comentarios, si los considera imprudentes, puede eliminarlos.
![Screenshot](../images/img-m-30-1.png)
Antes de eliminar un comentario el usuario tendrá que especificar el motivo de esta acción.
![Screenshot](../images/img-m-30-2.png)
5. El usuario puede __Editar__ publicaciones realizadas por otros usuarios haciendo click en los tres puntos que aparecen en la parte superior derecha de la publicación, esta acción nos llevará al formulario de edición donde se podrán realizar cambios, marcarlo como una publicación destacada y también cerrar la publicación, lo cual hará que los usuarios no puedan comentar esta publicación.
![Screenshot](../images/img-m-32.png)


## Actividad de repositorio

En este apartado se lista el registro de todos los cambios realizados por los usuarios a fin de contar con un historial de cambios a los datos referentes al arbolado urbano.
En la tabla se nos describe a detalle cada una de las actividades realizadas. 
![Screenshot](../images/img-m-13.png)

Cada actividad con una versión mayor a uno facilitará la observación de cambios entre la versión anterior y la nueva versión.
![Screenshot](../images/img-m-14.png)