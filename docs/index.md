# Árbol IoT

Árbol IoT es una solución tecnológica gamificada compuesta por una aplicación móvil, una aplicación web y un conjunto de sensores IoT, enfocada en la resolución de los siguientes objetivos estratégicos:

![Screenshot](images/img-1.png)


## Componentes

La solución se conforma de tres componentes principales, mismos que se describen a continuación:

### Árbol IoT Web

Mapa interactivo y backend donde puede consultarse el inventario de arbolado urbano de la ciudad, sus eco-beneficios y la administración de los módulos y contenidos existentes en la plataforma.

### Árbol IoT Móvil

La aplicación móvil integra técnicas, herramientas y dinámicas lúdicas, donde a través de la interacción con distintas funcionalidades, el / la usuaria pueden obtener recompensas basadas en motivaciones intrínsecas y extrínsecas.
 
## Arquitectura tecnológica de la solución

En este apartado se detallan las tecnologias utilizadas en la solución tecnológica Árbol IoT 2.0 y su arquitectura.

![Screenshot](images/img-3.png)

### Docker
Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos. Docker utiliza características de aislamiento de recursos del kernel Linux, tales como cgroups y espacios de nombres (namespaces) para permitir que "contenedores" independientes se ejecuten dentro de una sola instancia de Linux, evitando la sobrecarga de iniciar y mantener máquinas virtuales.

### Angular
Angular (comúnmente llamado "Angular 2+" o "Angular 2"), es un framework para aplicaciones web desarrollado en TypeScript, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página. Su objetivo es aumentar las aplicaciones basadas en navegador con capacidad de Modelo Vista Controlador (MVC), en un esfuerzo para hacer que el desarrollo y las pruebas sean más fáciles.

### React native
React Native es un framework que te permite construir aplicaciones móviles utilizando solamente JavaScript y React. Utiliza el mismo diseño que React.js, permitiéndole usar elementos de interfaz de usuario móvil. No es para construir una ‘aplicación móvil web’, una ‘aplicación de HTML5’ o una ‘aplicación híbrida’, sino una aplicación móvil real que es indistinguible de otra aplicación que usa construida en un lenguaje nativo. React Native utiliza los mismos bloques fundamentales de interfaz de usuario que las aplicaciones normales de iOS y Android.

### Nginx
Nginx es un servidor web/proxy inverso ligero de alto rendimiento y un proxy para protocolos de correo electrónico (IMAP/POP3).

### Spring
Spring es un framework para el desarrollo de aplicaciones y contenedor de inversión de control, de código abierto para la plataforma Java.

### Geoserver
GeoServer es un servidor de código abierto escrito en Java, el cual permite a los usuarios compartir y editar datos geoespaciales. Diseñado para la interoperabilidad, publica datos de las principales fuentes de datos espaciales usando estándares abiertos. GeoServer ha evolucionado hasta llegar a ser un método sencillo de conectar información existente a globos virtuales tales como Google Earth y NASA World Wind (véase así como mapas basados en web como OpenLayers, Google Maps y Bing Maps). GeoServer sirve de implementación de referencia del estándar Open Geospatial Consortium Web Feature Service, y también implementa las especificaciones de Web Map Service y Web Coverage Service.

### Minio
Minio es un servidor de almacenamiento de objetos y archivos.

### Postgresql
PostgreSQL es un sistema de gestión de bases de datos relacional orientado a objetos y de código abierto, publicado bajo la licencia PostgreSQL,​ similar a la BSD o la MIT.

### Postgis
PostGIS convierte al sistema de administración de bases de datos PostgreSQL en una base de datos espacial mediante la adición de tres características: tipos de datos espaciales, índices espaciales y funciones que operan sobre ellos. Debido a que está construido sobre PostgreSQL, PostGIS hereda automáticamente las características de las bases de datos empresariales, así como los estándares abiertos que implementan un Sistema de Información Geográfica dentro del motor de base de datos.



