# Guía de usuario general
Antes de entrar en materia acerca de las facultades a las que tiene acceso un usuario con rol asignado (pro, manager, administrador), se considera pertinente describir las atribuciones de un usuario anónimo y la distribución de componentes en la plataforma, esto para facilitar la localización de módulos y funcionalidades.

## Distribución general
Durante la navegación realizada dentro de la plataforma se distinguen cinco regiones principales:

1.  __Barra de navegación principal:__ Incluye un acceso directo a la página principal, así como las opciones de inicio de sesión, o el menú de usuario (después de haber iniciado sesión).
2.  __Braniding:__ Hace referencia a las organizaciones que impulsan el proyecto.
3.  __Menú secundario:__ Vínculos a los contenidos relevantes del sitio y que son de acceso público.
4.  __Contenido:__ Espacio donde se muestra la información o contenido principal de la página visitada.
5.  __Pie de página:__ Información referente a la construcción de la plataforma.

![Screenshot](../images/img-m-1.png)

Habiendo comprendido la distribución de la plataforma, a continuación, se comenzará a describir las interacciones que puede realizar un usuario anónimo en esta.

## Registro 

Este módulo o sección permite dar de alta nuevos usuarios para el uso tanto de la plataforma web como de la aplicación móvil "Árbol IoT".

Se puede acceder al formulario de Registro para crear una cuenta mediante dos métodos: 

1. Localizando la opción *"Crear una cuenta"* localizada en la barra de navegación principal, en la parte superior derecha de la pantalla
2. Al ingresar a la ruta principal del sitio agregando a la url  *"/register"*

Al ingresar se mostrará un formulario el cual se deberá completar para obtener una cuenta de usuario.

## Inicio de sesión

Se puede acceder al formulario de inicio de sesión mediante dos métodos: 

1. Localizando la opción *"Iniciar sesión"* localizada en la barra de navegación principal, en la parte superior derecha de la pantalla

2. Al ingresar a la ruta principal del sitio agregando a la url  *"/login"*

Una vez ingresados los datos de acceso, e iniciada la sesión, se visualizará información adicional en la barra de navegación, mostrando el nombre, el apellido y la imagen de perfil del usuario en caso de estar disponible, cualquiera de estos elementos permitirán desplegar el *"menú de usuario"*.

![Screenshot](../images/img-m-3.png)

Un usuario logueado en la plataforma puede tener alguno de los siguientes roles:

* Usuario / Usuario
* Usuario pro / Usuario pro
* Manager / Administrador de contenido
* Admin / Administrador general

A continuación se detallará cuáles son las actividades que puede realizar un usuario con rol de usuario.

## Menú de usuario
Este se desplegará al hacer click sobre la flecha localizada en el nombre de usuario o foto de perfil y mostrará las áreas adicionales a las cuales tendremos acceso.

![Screenshot](../images/img-m-4.png)


### Mi cuenta
En esta sección se muestra la información capturada al crear la cuenta y algunos datos adicionales que permiten mejorar la experiencia en el uso de la aplicación móvil Árbol IoT, esta interface tiene dos comportamientos:

1) Al solo ser usuarios de la plataforma.

![Screenshot](../images/img-m-5.png)

2) Al ser usuario de la plataforma y ser usuario activo en la aplicación móvil.

![Screenshot](../images/img-m-6.png)

* A través de la opción __Editar__ de esta interface se permite desplegar la interface de edición de cuenta.

### Contraseña

Este apartado permite realizar la actualización de la contraseña de acceso a la plataforma, para ello solo debemos capturar los campos solicitados y guardar los ajustes.

### Cerrar sesión
Este apartado permite finalizar la sesión activa. Al finalizar la sesión, automáticamente se redireccionara a la interface principal del sitio.

## Visor del arbolado
Este apartado se localiza en la página principal del sitio, podemos acceder a este contenido a través del menú secundario o mediante el icono "Home" localizado en la barra de navegación principal.

En este apartado se tendrá acceso y herramientas para interactuar con las fichas del arbolado urbano.

En esta pantalla identificaremos seis áreas:

![Screenshot](../images/img-m-8.png)

1. __Mapa:__ En este apartado podremos observar los puntos del arbolado urbano basados en la colorimetría descrita en el punto 3. Al hacer click sobre un punto podremos acceder a sus detalles en el área de *información descriptiva*
2. __Zoom control:__ Permite acercar o alejar la vista del mapa, este control también se puede realizar mediante el scroll del mouse
3. __Leyenda:__ Información que describe la colorimetría utilizada para la representación de datos en el mapa
4. __Buscador:__ Permite delimitar la información geográfica mostrada en el mapa a través de búsquedas por colonias o polígonos, también podemos realizar búsquedas sobre la información del arbolado urbano en este caso por especie
5. __Configuración y operaciones:__ Configuraciones avanzadas para el manejo de los datos a mostrar en el mapa, el usuario con este rol puede realizar las siguientes configuraciones:
    - __Capas__: Configurar la capa base del mapa, así como las capas de datos disponibles.
    ![Screenshot](../images/img-m-9.png)
    - __Filtros__: Filtros sobre la información del arbolado urbano.
    ![Screenshot](../images/img-m-10.png)
    - __Localizar__: Permite centrar el mapa de acuerdo a la georeferencia actual del dispositivo utilizado.
    - __Exportar__: Permite descargar un archivo de datos con la información del arbolado urbano, esta descarga responde a los filtros especificados en el visor. 
6. __Información descriptiva:__ Este apartado muestra los beneficios ecológicos que brinda el arbolado urbano, estos beneficios están condicionados a la información contextual y/o de búsqueda. A su vez al interactuar con el mapa nos mostrará la información del árbol en seleccionado y un enlace a la ficha detallada del mismo.
Al interactuar con el botón ver ficha nos enviará a una pantalla como la siguiente: 
    ![Screenshot](../images/img-m-8-1.png)
    Este usuario solo puede ver los datos y compartir en redes sociales.

## Comunidad
Puedes acceder a este apartado desde el menú secundario, al acceder podemos __Visualizar__ dos secciones:

1. __Acciones:__  Permite realizar filtrados textuales, por etiquetas o por indicador de publicaciones destacadas.
2. __Listado de publicaciones:__  Permite desplegar el listado de publicaciones, ordenadas por fecha de publicación. 

![Screenshot](../images/img-m-11.png)

El usuario puede acceder a la ficha de la publicación al hacer click en el icono de comentario.
![Screenshot](../images/img-m-12.png)

## Actividad de repositorio

En este apartado se lista el registro de todos los cambios realizados por los usuarios a fin de contar con un historial de cambios de los datos referentes al arbolado urbano.
En la tabla se nos describe a detalle cada una de las actividades realizadas. 
![Screenshot](../images/img-m-13.png)

Cada actividad con una versión mayor a uno facilitará la observación de cambios entre la versión anterior y la nueva versión.
![Screenshot](../images/img-m-14.png)