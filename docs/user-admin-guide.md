# Guía del administrador general
Un administrador general es aquel usuario que ha sido designado por la organización responsable del proyecto como un administrador técnico general, lo que le permite gestionar la creación de usuarios, asignar roles, supervisar contenidos generados por las aplicaciones móviles, acceder a logs del sistema así como administrar catálogos y reglas de gamificación.

Antes de entrar en materia acerca de las facultades a las que tiene acceso un usuario con este rol, se considera pertinente describir la distribución de componentes en la plataforma para facilitar la localización de módulos y funcionalidades.

## Distribución general
Durante la navegación realizada dentro de la plataforma se distinguen cinco regiones principales:

1.  __Barra de navegación principal:__ Incluye un acceso directo a al página principal, así como las opciones de inicio de sesión, o el menú de usuario (después de haber iniciado sesión).
2.  __Braniding:__ Hace referencia a las organizaciones que impulsan el proyecto.
3.  __Menú secundario:__ Vínculos a los contenidos relevantes del sitio y que son de acceso público.
4.  __Contenido:__ Espacio donde se muestra la información o contenido principal de la página visitada.
5.  __Pie de página:__ Información referente a la construcción de la plataforma.

![Screenshot](../images/img-m-1.png)

Habiendo comprendido la distribución de la plataforma, a continuación se comenzará a describir el proceso de acceso a la misma, así como las acciones que puede realizar sobre esta.

## Inicio de sesión

Se puede acceder al formulario de inicio de sesión mediante dos métodos: 

1. Localizando la opción *"Iniciar sesión"* localizada en la barra de navegación principal, en la parte superior derecha de la pantalla

2. Al ingresar a la ruta principal del sitio agregando a la url  *"/login"*

Una vez ingresados los datos de acceso, e iniciada la sesión, se __Visualizará__ información adicional en la barra de navegación, mostrando el nombre, el apellido y la imagen de perfil del usuario en caso de estar disponible, cualquiera de estos elementos permitirán desplegar el *"menú de usuario"*, y en caso de que el usuario tenga permisos para ello, se mostrará el acceso a otros apartados de la plataforma.

![Screenshot](../images/img-m-33.png)

Un usuario logueado en la plataforma, puede tener alguno de los siguientes roles:

* Usuario / Usuario
* Usuario pro / Usuario pro
* Manager / Administrador de contenido
* Admin / Administrador general

A continuación se detallará cuáles son las actividades que puede realizar un usuario con rol admin.

## Menú de usuario
Este se desplegará al hacer click sobre la flecha localizada en el nombre de usuario o foto de perfil y mostrará las áreas adicionales a las cuales tendremos acceso.

![Screenshot](../images/img-m-15.png)

### Mi cuenta
En esta sección se muestra la información capturada al crear la cuenta y algunos datos adicionales que permiten mejorar la experiencia en el uso de la aplicación móvil Árbol IoT, esta interface tiene dos comportamientos:

1) Al solo ser usuarios de la plataforma.

![Screenshot](../images/img-m-5.png)

2) Al ser usuario de la plataforma y ser usuario activo en la aplicación móvil.

![Screenshot](../images/img-m-6.png)

* A través de la opción __Editar__ de esta interface se permite desplegar la interface de edición de cuenta.

### <a name="validar"></a>Validar árboles
Mediante el uso de la aplicación móvil Árbol IoT, los usuarios pueden capturar o modificar la información referente al arbolado urbano, los usuarios con este rol podrán determinar si la información registrada es válida o no, así como realizar las modificaciones pertinentes. El flujo de este módulo se detalla a continuación:

* Listar árboles por validar

![Screenshot](../images/img-m-16.png)

* Seleccionar opción de validación y consulta de ficha del árbol

![Screenshot](../images/img-m-17.png)

* Modificación de estatus de ficha. 

![Screenshot](../images/img-m-18.png)

Existen tres estatus posibles para la ficha de árbol:

- __Incompleto__: Permite que el árbol pueda seguir siendo editado por cualquier usuario
- __Por validar__: Estatus asignado por un usuario a través de la aplicación móvil para solicitar la validación del mismo, este estatus bloquea futuras ediciones por parte de otros usuarios
- __Validado__: Estatus asignado por un usuario PRO o Manager una vez acreditan la validez de los datos registrados. Una vez en este estatus solo los usuarios con rango manager o superior podrán volver a __Editarlo__ en caso de ser necesario

### Crear trivia
Una trivia es un conjunto de preguntas que tienen la finalidad de alimentar al módulo de trivias de la aplicación móvil, cada una de ellas es de opción múltiple y se crean como se detalla a continuación:

1.&nbsp;Leer los requisitos previos a la creación de una trivia y seleccionar continuar

![Screenshot](../images/img-m-19.png)

2.&nbsp;Capturar el conjunto de preguntas requeridas siguiendo las instrucciones del formulario de captura

![Screenshot](../images/img-m-20.png)

Podrás navegar entre las preguntas creadas para modificar algún dato en caso de ser necesario

![Screenshot](../images/img-m-21.png)

### Ascender usuarios

En este apartado se lista a los usuarios que interactúan en la aplicación móvil Árbol IoT, identificando dos áreas principales:

![Screenshot](../images/img-m-22.png)

1. __Área de filtrado__: Permite decidir si queremos ver a los usuarios que ya fueron promovidos a usuario PRO o consultar a los posibles candidatos
2. __Listado de usuarios__: Listado de usuarios con botón de acción para ascender a los mismos

Al presionar el botón desde el área el listado de candidatos se desplegará una alerta donde se deberá aceptar que se le asignarán nuevas atribuciones al usuario en cuestión


![Screenshot](../images/img-m-23.png)

Al presionar el botón desde el listado de usuarios PRO, se podrá remover las atribuciones de usuario convirtiéndolo en un usuario normal


![Screenshot](../images/img-m-24.png)

Al degradar a un usuario se deberá especificar el motivo por el cual se ha realizando la acción


![Screenshot](../images/img-m-25.png)


### Contraseña

Este apartado permite realizar la actualización de la contraseña de acceso a la plataforma, para ello solo debemos capturar los campos solicitados y guardar los ajustes.

### Cerrar sesión
Este apartado permite finalizar la sesión activa. Al finalizar la sesión, automáticamente se redireccionará a la interface principal del sitio.

* Con esto finalizamos de describir las áreas a las cuales se tiene acceso a través del menú de usuario.

## Panel de administración

Una de las principales atribuciones de este tipo de usuarios es la de administación de contenido y la administración de la plataforma.

![Screenshot](../images/img-m-102.png)

A continuación se detallará cada uno de los módulos disponibles y sus principales características.


### Administración de contenido

Este apartado agrupa a los contenidos que se visualizan a través de la aplicación móvil, así como los contenidos auxiliares y sus relaciones, estas relaciones se analizarán en su momento cuando se revise cada uno de los diferentes módulos.

#### Arbolado urbano

__Catálogo de especies__

En este módulo podemos crear de manera individual cada uno de los registros de especies de árboles que son comunes para el área geográfica de implementación.
Este contenido se relaciona al catálogo de árboles. Gracias a la especie se podrá hacer un análisis más completo de los tipos de árboles que existen a través del *Visor del arbolado*

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.

![Screenshot](../images/img-m-35.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura, donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo especie.
![Screenshot](../images/img-m-36.png)
2. __Resumen del contenido__: Vista previa de la información contenida para dicho registro.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: Desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-37.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario, considerar que el campo 'Código' de esta entidad deberá corresponder con el código de especie de iTree-Streets para que se permita el cálculo de eco-beneficios para dicha especie.

    ![Screenshot](../images/img-m-38.png)
    - __Eliminar__: Eliminar el registro, considerar que en caso de que dicho contenido sea referenciado por otra entidad, no se permitiá efectuar el borrado.
    ![Screenshot](../images/img-m-39.png)

__Catálogo de árboles__

En este módulo podremos gestionar los contenidos referentes al arbolado urbano, estos registros corresponden a cada uno de los puntos desplegados en el mapa del *Visor del arbolado*. 

* Este contenido requiere del catálogo de especies.
* Este contenido es requerido por el módulo adopciones. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.

![Screenshot](../images/img-m-40.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo árbol.
![Screenshot](../images/img-m-41.png)
Este contenido cuenta con un campo de control llamado  *"Estatus"* que como se mencionó en el apartado de *Validación de árboles* permite seleccionar entre 3 estatus, para obtener más detalles revisar el apartado de validación de árboles

    ![Screenshot](../images/img-m-42.png)

2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: Desplegado de la información no editable, esta visualización es pública, puede ser consultada al inspeccionar la ficha del árbol desde el *Visor del arbolado*. 
    ![Screenshot](../images/img-m-43.png)
    En caso de que el árbol cuente con los metadatos de diámetro y especie, y la especie esté considerada dentro del modelo de iTree Streets se desplegarán los eco-beneficios proporcionados para dicho árbol. En caso de ser un usuario de tipo PRO, Manager o Admin se mostrará el botón de edición.
    ![Screenshot](../images/img-m-43-1.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-44.png)
    - __Eliminar__: Eliminar el registro, considerar que es un contenido referenciable, por lo cual, si existen otras entidades referenciando a este árbol no se efectuará el borrado.
    ![Screenshot](../images/img-m-45.png)

__Validar árboles__

Acceso directo al apartado de validación de árboles descrito anteriormente, [Ir a apartado.](#validar)

#### Páginas y bloques

__Páginas básicas__

En este módulo podremos gestionar los contenidos de tipo página, estas páginas son de carácter informativa.
Para dar a conocer estas páginas informativas haremos uso del contenido "Bloque".

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-97.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo página básica. Como consideraciones especiales, contemplar que en el campo contenido podremos escribir y dar formato al texto a publicar, entre las herramientas de ayuda para dar formato al texto encontramos la opción "Fuente html", podemos hacer uso de esta herramienta si se tiene conocimiento de la estructura HTML y en caso de querer agregar estructuras más complejas se puede hacer uso del Framework Bootrstrap v4. Otra consideración es que el editor nos permite agregar imágenes vía URL, aunque en ocasiones estas imágenes pueden ser de creación propia y no tener dónde publicarla para tomar el URL, podemos hacer click en el botón "Seleccionar imagen" para cargar la imagen a la plataforma y poder hacer uso de ella.
![Screenshot](../images/img-m-98.png)
2. __Resumen del contenido__: Vista previa de la información contenida para dicho registro.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: Desplegado de la información no editable, esta visualización es de acceso público.
    ![Screenshot](../images/img-m-99.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-100.png)
    - __Eliminar__: Eliminar el registro, considerar que es un contenido referenciable a través del contenido de tipo  *Bloque*, por lo cual si existen datos referenciando a este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-101.png)

__Bloques__

En este módulo podremos gestionar bloques de información, estos bloques se mostrarán solamente en las regiones antes definidas como "Branding", "Menú secundario" y "Pie de página". 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-105.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo Bloque.
    ![Screenshot](../images/img-m-104.png)

    Expliquemos brevemente el objetivo de algunos campos:

    - __Tipo__: 
        - Logo (imagen con enlace a otro apartado)
        - Enlace (texto o icono con enlace a otro apartado)
        - Texto (texto simple).
    - __Región__: Esta es la distribución que nos ofrece de acuerdo a la opción seleccionada.
        ![Screenshot](../images/img-m-103.png)
        - Header left (A)
        - Header right (B)
        - Header menu (C)
        - Footer left (D)
        - Footer menu (E)
        - Footer extra (F)
        - Footer right (G)

    - __Etiqueta__: Texto a mostrar en caso de no cargar logo o enlace o el valor  si se seleccionó texto
    - __Peso__: Al agregar varios elementos en una misma región este atributo nos permite determinar el orden en que aparecen mientas más bajo sea el valor más prioritario será por lo que se desplegará primero.

2. __Resumen del contenido__: Vista previa de la información contenida para dicho registro.
3. __Operaciones sobre el contenido__:
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-106.png)
    - __Eliminar__: Eliminar el registro, considerar que es un contenido referenciable a través del contenido de tipo  *Bloque*, por lo cual si existen datos referenciando a este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-107.png)


#### Límite geográfico

__Catálogo de límites geográficos__

En este módulo podremos gestionar los contenidos referentes a límites geográficos, estos registros corresponden a las áreas geográficas por las cuales podremos realizar búsquedas en el *Visor del arbolado*. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.

![Screenshot](../images/img-m-46.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo límite geográfico.
![Screenshot](../images/img-m-47.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: Desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-48.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-49.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-50.png) 

__Límites geográficos de usuario__

En este módulo podremos gestionar los contenidos referentes a límites geográficos de los usuarios, estos registros corresponden a las áreas geográficas que los usuarios del aplicativo móvil Árbol IoT han seleccionado como favoritas.
Este contenido tiene relación con usuarios y límites geográficos.


El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-108.png)

1. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
2. __Operaciones sobre el contenido__:
    - __Visualizar__: Desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-109.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-110.png) 

#### Reportes

__Reportes__

En este módulo podremos gestionar los contenidos referentes a los reportes sobre el arbolado realizados por los usuarios del aplicativo móvil Árbol IoT.
Este contenido necesita del catálogo de usuarios, límites geográficos y del catálogo con los tipos de reportes. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-51.png)
Podemos notar que en la parte superior tenemos un buscador *B*, lo cual permitirá a los usuarios manager delimitar y mostrar solo los reportes que le han sido asignados a él.

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo reporte.
![Screenshot](../images/img-m-52.png)
Este contenido cuenta con dos campos de control, el campo *Usuario asignado*; permitiendo asignar a un responsable y un campo *Estatus*; en el cual tenemos las opciones de marcar al reporte como Abierto y Cerrado en el entendido de que si se cierra un reporte indica que ya fue atendido.
![Screenshot](../images/img-m-53.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    - __Eliminar__: Elimina el registro.

__Tipos de reportes (etiquetas)__

En este módulo podremos gestionar las etiquetas referentes a los reportes.
Este contenido es necesario para crear contenidos de tipo reporte. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-111.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo de reporte.
![Screenshot](../images/img-m-112.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-113.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-114.png)
    - __Eliminar__: Elimina el registro, considerar que esté en un contenido referenciable desde contenidos de tipo *Reporte* por lo cual si existen datos utilizando esta etiqueta no se efectuará el borrado.
    ![Screenshot](../images/img-m-115.png)

#### Trivias
 
__Preguntas de trivia__

En este módulo podremos gestionar los contenidos referentes a las trivias, los cuales son preguntas que serán puestas a disposición de los usuarios del aplicativo móvil Árbol IoT. A diferencia del apartado de *Crear trivia* que encontramos en el menú del usuario, en este apartado se pueden gestionar las preguntas de manera individual y no en paquetes de cinco. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-54.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo reporte. Este contenido cuenta con un campo de control *Activar pregunta*; para evitar que la pregunta se muestre sin la necesidad de eliminarla. 
![Screenshot](../images/img-m-55.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-56.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-57.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-58.png)

__Respuestas de usuario__

En este módulo podremos gestionar las respuestas a las preguntas de trivia contestadas por los usuarios a través del aplicativo móvil Árbol IoT.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-116.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos para crear el nuevo contenido.
![Screenshot](../images/img-m-117.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-118.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-119.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-120.png)

#### Comunidad

__Publicaciones__

En este módulo podremos gestionar las publicaciones del apartado de comunidad las cuales son capturadas y visualizadas por los  usuarios del aplicativo móvil Árbol IoT o usuarios pro mediante el apartado Comunidad ubicado en el *menú secundario*. 
Este contenido tiene relación con el contenido *Tipos de publicaciones (etiquetas)* y con los usuarios.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-59.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo reporte.
Este contenido cuenta con dos campos de control, el campo *Mostrar como destacado* y el campo *Estatus*; El primero para denotar que la publicación es realmente interesante y realizar una segregación con respecto al resto, y el segundo nos permitirá habilitar o cerrar la opción de comentarios por parte de los usuarios para esta publicación.
![Screenshot](../images/img-m-60.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización es de acceso público, como se mencionó anteriormente en la sección de comunidad, los usuarios con rol pro, manager o admin, pueden interactuar con esta interfaz, mediante los 3 puntos para la edición, dar me gusta a la publicación o bien escribir un comentario al respecto.
    ![Screenshot](../images/img-m-61.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-62.png)
    - __Eliminar__: Elimina el registro, considerar que es un contenido referenciable por lo cual si existen datos utilizando este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-63.png)

__Comentarios de publicaciones__

En este módulo podremos gestionar los comentarios referentes a las publicaciones del apartado de Comunidad, estos comentarios son realizados por los usuarios del aplicativo móvil Árbol IoT o por los usuarios con rol pro, manager o admin a través de la plataforma. Este apartado facilita la revisión de los comentarios.
Este contenido tiene relación con usuarios y publicaciones.

Este acceso directo nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir uno o dos apartados dependiendo de si existe o no información de este tipo.
![Screenshot](../images/img-m-64.png)

1. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
2. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-65.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-66.png)
    - __Eliminar__: Elimina el registro. Para realizar esta acción es importante realizar los siguientes pasos:
        - Visitar la publicación dando click en el enlace a publicación de la tabla
        ![Screenshot](../images/img-m-67.png)
        Esto nos enviará a la vista pública de la publicación permitiendo tener un contexto informado sobre el comentario, permitiendo así determinar si este es relevante o no, en caso de no serlo, en la parte superior derecha del comentario podremos eliminarlo
    ![Screenshot](../images/img-m-68.png)
    Para lo cual nos pedirá definir el motivo por el cual se lleva a cabo esta acción
    ![Screenshot](../images/img-m-69.png)

__Tipos de publicaciones (etiquetas)__

En este módulo podremos gestionar las etiquetas que se mostrarán para categorizar las publicaciones en la sección de comunidad.
Este contenido es necesario para crear contenidos de tipo publicación. 

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-121.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipos de publicación.
![Screenshot](../images/img-m-122.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-123.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-124.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-125.png)

__Likes de usuarios__

En este módulo podremos gestionar los eventos de tipo Like que han realizado los usuarios del aplicativo Árbol IoT en las publicaciones de comunidad o los usuarios de rol pro, manager y admin desde la sección de comunidad de la plataforma web.

Este acceso directo nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir uno o dos apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-126.png)

1. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
2. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-127.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-128.png)

#### Notificaciones

__Notificaciones__

En este módulo podremos gestionar las notificaciones push que reciben los usuarios a través de la aplicación móvil.

* Este contenido está relacionado con los usuarios.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-70.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura. Este contenido cuenta con tres campos de control: 
    - *Tipo*: nos indica si la notificación se mostrará solo en la bandeja de notificaciones del usuario "Segundo plano" o en una alerta dentro de la aplicación móvil "Primer plano".
    - *Subtipo*: nos indica el origen de la notificación (proceso que desencadena la notificación enviada), seleccionar default para el envío de notificaciones manuales.

    ![Screenshot](../images/img-m-71.png)

2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-72.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-73.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-74.png)

__Notificaciones geográficas__

A través de este módulo podremos gestionar notificaciones geográficas que los usuarios recibirán a través de la aplicación móvil una vez entren al polígono configurado.
Estas notificaciones a diferencia de las anteriores las recibirán todos aquellos usuarios que se encuentren dentro de un polígono determinado. Con ella puedes notificar a los usuarios de la aplicación sobre campañas o eventos referentes al arbolado urbano que se encuentren en las cercanías del lugar.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-75.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura. Este contenido cuenta con tres campos de control: 
    - *Tipo*: nos indica si la notificación se mostrará solo en la bandeja de notificaciones del usuario "Segundo plano" o en una alerta dentro de la aplicación móvil "Primer plano".
    - *Subtipo*: nos indica el origen de la notificación (proceso que desencadena la notificación enviada), seleccionar "Default" para el envío de notificaciones manuales.

    ![Screenshot](../images/img-m-76.png)

2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-77.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-78.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-79.png)


#### Gamificación

__Recurso gráfico de gamificación__

En este módulo podremos gestionar todos aquellos recursos gráficos que son necesarios para el aplicativo móvil Árbol IoT.
Este contenido es necesario para el contenido logros.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-129.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura.
 En este formulario podremos cargar imágenes o generar una a través de un asistente, actualmente la primera opción es utilizada para cargar las imágenes utilizadas para representar el nivel del usuario en la aplicación móvil y la segunda opción para generar las imágenes de las insignias que pueden ganar estos usuarios.
 ![Screenshot](../images/img-m-130.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-131.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-132.png)
    - __Eliminar__: Elimina el registro, considerar que es un contenido referenciable por lo cual si existen datos utilizando este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-133.png)

__Logros__

En este módulo podremos gestionar los tipos de logros que pueden realizar los usuarios mediante el uso del aplicativo móvil Árbol IoT.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-134.png)

1. __Creación de contenido__: Al seleccionar esta opción se desplegará el formulario de captura donde bastará con seguir las instrucciones y restricciones descritas en el formulario, una vez capturada la información, bastará con guardar el nuevo contenido de tipo logro.
 ![Screenshot](../images/img-m-135.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-136.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-137.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-138.png)

__Escala de puntos__

En este módulo podremos gestionar escalas de puntos que utiliza el aplicativo móvil Árbol IoT.
Este módulo es requerido por el módulo de perfiles de puntos.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-139.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos para crear una nueva escala de puntos.
 ![Screenshot](../images/img-m-140.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-141.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-142.png)
    - __Eliminar__: Elimina el registro, considerar que es un contenido referenciable por lo cual si existen datos utilizando este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-143.png)

__Reglas de gamificación__

En este módulo podremos gestionar las reglas de gamificación que utiliza el aplicativo móvil Árbol IoT.
Este contenido es el encargado de definir que tiene que hacer el usuario para obtener logros, o cuantos puntos puede obtener por cada acción realizada.
Este contenido cuenta con un campo de control, el campo "Activo" que nos permitirá habilitar o deshabilitar reglas según lo consideremos pertinente.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-144.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura. Este formulario se llena en dos pasos:
    - Definir la información básica y guardar
    ![Screenshot](../images/img-m-145.png) 
    - Realizar la codificación de la regla. En este segundo paso tendremos acceso al campo "Activo" que nos permitirá habilitar o deshabilitar reglas según lo consideremos pertinente.
    ![Screenshot](../images/img-m-146.png)
    - El campo listener permite determinar bajo qué evento se ejecutará el código JavaScript definido en la regla.
    - El código de la regla se define en JavaScript, la plantilla base de Árbol IoT cuenta con más de 20 reglas de gamificación que permiten entender cómo codificar una nueva regla.

2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario. 
    ![Screenshot](../images/img-m-147.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-148.png)

__Tutoriales__

En este módulo podremos gestionar los diversos tutoriales que pueden ser lanzados a través de las reglas de gamificación y que a su vez se muestran a los usuarios del aplicativo móvil Árbol IoT.
Actualmente se cuenta con un tutorial por nivel de usuario, un tutorial es una sucesión de slides que describen las nuevas actividades que puede realizar el usuario a partir de ese nivel.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-149.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos para crear un nuevo tutorial.
 ![Screenshot](../images/img-m-150.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-151.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-152.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-153.png)

__Perfiles de gamificación__

En este apartado podremos gestionar los perfiles de gamificación. Este perfil es creado cuando un usuario se registra en el aplicativo móvil Árbol IoT, su objetivo principal es el de gestionar todo lo referente a la dinámica de juego que sigue la aplicación, niveles, logros y puntos de usuario.

&nbsp;

- Este contenido tiene relación con la entidad usuario, límite geográfico y logros.

- Este contenido es necesitado por la entidad perfiles de puntos.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-154.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos.
 ![Screenshot](../images/img-m-155.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-156.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-157.png)
    - __Eliminar__: Elimina el registro, considerar que es un contenido referenciable por lo cual si existen datos utilizando este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-158.png)

__Perfiles de puntos__

En este apartado podremos gestionar los perfiles de puntos, perfil que es creado cuando un usuario se registra en el aplicativo móvil Árbol IoT y que tiene el objetivo de gestionar el almacenado de puntos de usuario.
&nbsp;

- Este contenido tiene relación con el perfil de gamificación, sistema de puntos.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-159.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos.
 ![Screenshot](../images/img-m-160.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-161.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-162.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-163.png)

__Logros de usuario__

En este apartado podremos gestionar los logros que han obtenido los usuarios del aplicativo móvil Árbol IoT.
Este contenido tiene relación con la entidad usuario y logros.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-164.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos.
 ![Screenshot](../images/img-m-165.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-166.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-167.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-168.png)

__Adopciones de usuario__

En este apartado podremos gestionar las adopciones del arbolado urbano que han realizado los usuarios del aplicativo móvil Árbol IoT.
Este contenido tiene relación con la entidad usuario y árboles.

Este acceso directo nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir uno o dos apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-169.png)

1. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
2. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-170.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-171.png)

__Seguidores__

En este apartado podremos gestionar las relaciones de los usuarios del aplicativo móvil Árbol IoT.
Este contenido tiene relación con la entidad usuario.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-172.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos.
 ![Screenshot](../images/img-m-173.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-174.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-175.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-176.png)

#### Sensores y mediciones
&nbsp;

__Dispositivo IoT__
Este apartado nos permite registrar los dispositivos IoT a través de los cuales se está recolectando información del medio ambiente.
Este contenido es necesario para el contenido de tipo medición y medición legible.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-80.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura. Este contenido cuenta con tres campos de control: 
    - *Código*: Es el código de referencia que será registrado en las plataformas de los dispositivos IoT para enlazar ambas plataformas al momento de realizar actualizaciones.
    - *Última sincronización*: Campo usado por el sistema. Al crear el contenido almacenar la fecha en que fue creado el contenido.
    - *Activo*: Permite activar la recepción de mediciones desde dicho sensor.
    ![Screenshot](../images/img-m-81.png)

2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-82.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario. Evitar modificar el campo *Última sincronización*
    ![Screenshot](../images/img-m-83.png)
    - __Eliminar__: Elimina el registro, considerar que es un contenido referenciable por lo cual si existen datos utilizando este contenido no se efectuará el borrado.
    ![Screenshot](../images/img-m-84.png)

__Medición__

Este apartado nos permite realizar una copia de la trama original recibida desde SigfoxCloud. Es común que la información en estos sistemas esté almacenada en tramas hexadecimales, no legibles para los usuarios.
Este contenido necesita del contenido Dispositivo IoT.
![Screenshot](../images/img-m-85.png)

__Medición legible__

Este apartado almacena la información IoT ya procesada, esta información posteriormente será utilizada para realizar gráficos sobre la situación medio ambiental.
Este contenido necesita del contenido Dispositivo IoT.
![Screenshot](../images/img-m-86.png)

#### Contenido relacionado

__Metadatos de entidad__

En este apartado podremos gestionar los eventos realizados por la interacción entre entidades, este contenido es de uso del sistema.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-177.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura en el cual seguimos las instrucciones de llenado y guardamos.
 ![Screenshot](../images/img-m-178.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-179.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-180.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-181.png)


### Administración de la plataforma

#### Configuración avanzada

Para concer más detalles, revisar el apartado de instalación 'Actualización de datos generales de instancia' 

#### Configuración de entorno

Este módulo nos permite desplegar las variables de entorno utilizadas por Árbol IoT
![Screenshot](../images/img-m-188.png)

#### Gestión de usuarios

Este módulo ayuda a la gestión de usuarios. Donde el admin puede crear cuentas de administración, este apartado nos permite agregar diractamente una contraseña que luego el usuario podrá cambiar, para la asignación de roles considerar lo siguiente:

* Usuario / Usuario
* Usuario pro / Usuario pro
* Manager / Administrador de contenido
* Admin / Administrador general

La cuenta puede ser activada o descativada a través del campo  "Activo".

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-189.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura de cuentas.
 ![Screenshot](../images/img-m-190.png)
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: desplegado de la información no editable, esta visualización no es de acceso público.
    ![Screenshot](../images/img-m-191.png)
    - __Editar__: Acceso al formulario de edición para actualizar o corregir la información en caso de ser necesario.
    ![Screenshot](../images/img-m-192.png)
    - __Eliminar__: Elimina el registro.
    ![Screenshot](../images/img-m-193.png)

#### Métricas

Este acceso directo nos redireccionará a la ficha con las métricas de rendimiento de la solución.
![Screenshot](../images/img-m-194.png)

#### Salud

Este acceso directo nos redireccionará a la ficha con el estado de salud de la solución.
![Screenshot](../images/img-m-195.png)
![Screenshot](../images/img-m-196.png)

#### Auditorías

Este acceso directo nos redireccionará a la ficha con el listado de eventos de inicio de sesión detectados por el sistema. Podremos interactuar mediante el buscador para filtrar la información por rango de fechas.
![Screenshot](../images/img-m-197.png)

#### Auditorías de entidad

Este acceso directo nos redireccionará a una apartado de búsquedas, donde permitirá seleccionar la entidad que queremos auditar y el número de resultados que queremos __Visualizar__.
Esta auditoría es referente a las modificaciones realizadas por los usuarios a los contenidos.
![Screenshot](../images/img-m-198.png)

#### Logs

Este acceso directo nos redireccionará a la ficha de encendido y apagado de logs.
![Screenshot](../images/img-m-199.png)

#### Campos personalizados

Este acceso directo nos redireccionará a la ficha de configuración de campos personalizados, estos campos actuarán sobre la entidad árbol, permitiendo añadir más características a tomar en cuenta para obtener mayores datos sobre el arbolado urbano.
![Screenshot](../images/img-m-200.png)

#### Importaciones 
Este módulo permite realizar importaciones batch, estas importaciones serán contenidos de tipo árbol, especie y límites geográficos.

El acceso directo de este módulo  nos redireccionará al listado de contenidos, en esta interfaz podremos distinguir entre dos o tres apartados dependiendo si existe o no información de este tipo.
![Screenshot](../images/img-m-87.png)

1. __Creación de contenido__: Al seleccionar esta opción nos desplegará el formulario de captura el cual funciona de la siguiente manera:
    ![Screenshot](../images/img-m-88.png)

    - Seleccionar "tipo" de entidad que queremos insertar en la base de datos y seleccionar "Juego de caracteres".
    Al hacer esto nos desplegará la plantilla; archivo solo con las cabeceras de los campos en el orden de lectura requerido y el diccionario de datos; archivo que explica la cabecera de la plantilla, el tipo de dato que recibe y las restricciones de almacenamiento, es decir si el campo es obligatorio o si debe de llevar otras consideraciones.
    - Estandarizar datos.
    - Subir nuestro archivo con la información estandarizada. 
    - Seleccionar "Procesar archivo". Al iniciar el procesamiento del archivo la interfaz cambiará y nos mostrará el proceso de carga de los datos,
    ![Screenshot](../images/img-m-89.png)

    Si la carga fue exitosa obtendremos la siguiente interfaz: 
    ![Screenshot](../images/img-m-90.png)

    Si existió algún error durante el proceso de carga nos mostrará la siguiente interfaz:
    ![Screenshot](../images/img-m-91.png)
    a) Resultados del procesamiento

    b) Archivo solo con los registros que tienen errores

    c) Descripción de cuál fue el error y en qué fila del archivo original se encontraba.
    
2. __Resumen del contenido__: Información contenida en el registro desplegada a manera de vista previa.
3. __Operaciones sobre el contenido__:
    - __Visualizar__: nos mostrará la ficha del resultado de la importación.

#### Asistente de selección de especie

Este módulo tiene el objetivo de facilitar el registro del arbolado urbano a través de un asistente de selección de especie, el cual funciona a partir de las descripciones y atributos del arbolado urbano modelados a través de este módulo.

* Mientras más nivel de detalle tenga la información capturada en este apartado, mayor será la calidad de la información registrada por los usuarios.

* Este módulo requiere del contenido de tipo especie.

![Screenshot](../images/img-m-92.png)
![Screenshot](../images/img-m-93.png)

Este acceso directo nos redireccionará al listado, del lado izquierdo encontraremos las características y del lado derecho las operaciones entre las cuales podemos: 

&nbsp;

1. __Eliminar__: Borrar el elemento, se recomienda no borrar los elementos padres.
![Screenshot](../images/img-m-94.png)

2. __Editar__: Modificar la información, cambiar de imagen o título.
![Screenshot](../images/img-m-95.png)

3. __Añadir__: Añade nuevos elementos
    ![Screenshot](../images/img-m-96.png)
    * Esta presentación puede variar según el nivel de inmersión en el que se encuentre.
 
4. __Desplegar / contraer__: Muestra u oculta las características que se desprenden del nivel actual

#### API

Este acceso directo nos redireccionará al listado de servicios REST que son utilizados por la plataforma y por el aplicativo móvil Árbol IoT.
![Screenshot](../images/img-m-201.png)

## Visor del arbolado
Este apartado se localiza en la página principal del sitio, podemos acceder a este módulo a través del menú secundario o mediante el icono "Home" localizado en la barra de navegación principal.

En este apartado se tendrá acceso y herramientas para interactuar con el arbolado urbano.

Esta interface se conforma de las siguientes regiones principales:

![Screenshot](../images/img-m-27.png)

1. __Mapa:__ En este apartado podremos observar los puntos del arbolado urbano basados en la colorimetría descrita en el punto 3. Al hacer click sobre un punto podremos acceder a sus detalles en el área de *información descriptiva*
2. __Zoom control:__ Permite acercar o alejar la vista del mapa, este control también se puede realizar mediante el scroll del mouse
3. __Leyenda:__ Información que describe la colorimetría utilizada para la representación de datos en el mapa
4. __Buscador:__ Permite delimitar la información geográfica mostrada en el mapa a través de búsquedas por colonias o polígonos, también podemos realizar búsquedas sobre la información del arbolado urbano en este caso por especie.
5. __Configuración y operaciones:__ Configuraciones avanzadas para el manejo de los datos a mostrar en el mapa, el usuario con este rol puede realizar las siguientes configuraciones.
    - __Agregar árbol__: Brinda acceso al formulario de captura en el cual se solicitará información como la especie del árbol, el diámetro del tronco, el diámetro de la copa, la altura, la georeferencia, el estatus de la información y una foto del árbol.
    ![Screenshot](../images/img-m-28.png)
    - __Capas__: Configurar la capa base del mapa, así como las capas de datos disponibles.
    ![Screenshot](../images/img-m-9.png)
    - __Filtros__: Filtros sobre la información del arbolado urbano.
    ![Screenshot](../images/img-m-10.png)
    - __Localizar__: Permite centrar el mapa de acuerdo a la georeferencia actual del dispositivo utilizado.
    - __Exportar__: Permite descargar un archivo de datos con la información del arbolado urbano, esta descarga responde a los filtros especificados en el visor. 
6. __Información descriptiva:__ Este apartado muestra los beneficios ecológicos que brinda el arbolado urbano, estos beneficios están condicionados a la información contextual y/o de búsqueda. A su vez al interactuar con el mapa nos mostrará la información del árbol en seleccionado y un enlace a la ficha detallada del mismo.
Al interactuar con el botón ver ficha nos enviará a una pantalla como la siguiente; 
    ![Screenshot](../images/img-m-26.png)
    En este ejemplo se pueden ver los datos de la ficha del árbol, compartir en redes sociales y de considerarlo necesario __Editar__ la información del mismo.


## Comunidad
Puedes acceder a este apartado desde el menú secundario, al acceder podemos __Visualizar__ dos secciones:

![Screenshot](../images/img-m-29.png)

1. __Acciones:__  Permite realizar filtrados textuales, por etiquetas o por indicador de publicaciones destacadas.
2. __Listado de publicaciones:__  Permite desplegar el listado de publicaciones, ordenadas por fecha de publicación. 

A diferencia del usuario normal, el usuario PRO puede tener mayor interacción con la sección *Comunidad*, desde este apartado puede realizar las siguientes actividades:
![Screenshot](../images/img-m-31.png)

1. Crear una publicación en la comunidad, al entrar a la sección de Comunidad y posteriormente hacer click en el icono de captura localizado en el área de *acciones*, esto dará acceso al formulario de captura.
![Screenshot](../images/img-m-30.png)
2. El usuario puede dar me gusta a la publicación.
3. El usuario puede acceder a la ficha de la publicación al hacer click en el icono de comentario.
4. En la ficha de la publicación, el usuario podrá dejar comentarios al respecto.
En caso de existir comentarios, si los considera imprudentes, puede eliminarlos.
![Screenshot](../images/img-m-30-1.png)
Antes de eliminar un comentario el usuario tendrá que especificar el motivo de esta acción.
![Screenshot](../images/img-m-30-2.png)
5. El usuario puede __Editar__ publicaciones realizadas por otros usuarios haciendo click en los tres puntos que aparecen en la parte superior derecha de la publicación, esta acción nos llevará al formulario de edición donde se podrán realizar cambios, marcarlo como una publicación destacada y también cerrar la publicación, lo cual hará que los usuarios no puedan comentar esta publicación.
![Screenshot](../images/img-m-32.png)


## Actividad de repositorio

En este apartado se lista el registro de todos los cambios realizados por los usuarios a fin de contar con un historial de cambios a los datos referentes al arbolado urbano.
En la tabla se nos describe a detalle cada una de las actividades realizadas. 
![Screenshot](../images/img-m-13.png)

Cada actividad con una versión mayor a uno facilitará la observación de cambios entre la versión anterior y la nueva versión.
![Screenshot](../images/img-m-14.png)