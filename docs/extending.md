# Extensión y customización de la solución

La solución tecnológica Árbol IoT 2.0 se construye a partir del proyecto JHipster, un generador de aplicaciones de código abierto que se utiliza para desarrollar rápidamente aplicaciones web modernas utilizando Angular y Spring Framework. El utilizar Spring Framework facilita la documentación en código y provee un marco de desarrollo fácil de entender y extender gracias a la utilización de patrones de diseño probados a través del tiempo.

### Patrones de diseño utilizados

1. Dependency injection or inversion of control (IOC)
2. Factory Design Pattern
3. Proxy Design Pattern
4. Singleton Design Pattern
5. Model View Controller (MVC)
6. Front Controller Design Pattern
7. View Helper
8. Template method
9. DAO / DTO

### Tecnologías del stack

- Spring Boot
- Maven 
- Spring Security
- Spring MVC REST + Jackson
- Spring Data JPA + Bean Validation
- Database updates with Liquibase
- Angular 4

### Descripción del diseño de software

Para conocer más detalles de la arquitectura de la solución, solicitar acceso a la __'Descripción del diseño de software'__ y al __'código fuente de la solución'__ a los administradores de la solución.

### Docker registry

La instalación integrada a este paquete utiliza imagenes Docker para el desplegado de los contenedores, considerar que la imagen oficial del compilado de Árbol IoT 2.0 se encuentra en el siguiente
registro:

- registry.gitlab.com/coderobot-giz/biot-2.0

Solo los administradores de Árbol IoT pueden realizar actualizaciones sobre dicho repositorio, para actualizar la imagen antes mencionada (como administrador) o crear una imagen privada con tu personalización puedes utilizar el siguiente comando:

```
./mvnw  package -Pprod -DskipTests=true  jib:build
```
- Ejecutar comando en la raíz del proyecto de Árbol IoT Web

### Documentación relacionada

- https://www.jhipster.tech/development/