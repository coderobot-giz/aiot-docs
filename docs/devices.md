# Sensores IoT

Árbol IoT 2.0 puede alimentarse de variables medio ambientales sensadas por dispositivos IoT, ya sean dispositivos
DIY o profesionales, estos pueden ser conectados a Árbol IoT y visualizarse a través de los dashboards y capas base incluidos
en la solución.

Árbol IoT 2.0 considera las siguientes variables ambientales:

- Temperatura
- Humedad
- Ruido ambiental
- Partículas suspendidas 2.5u
- Partículas suspendidas 10u
- Gases NOX

## Diseño de tramas

Antes de conectar un dispositivo IoT, considerar que el diseño de tramas de Árbol IoT sigue el esquema de tramas de SIGFOX, considerando 12 bytes por mensaje 
y 4 tipos de mensajes, el tipo de mensaje se especifica en los primeros 2 bits de la trama.

En la siguiente figura se muestran los diferentes tipos de tramas así como la posición de la información de cada sensor dentro de la misma:

![Screenshot](../images/img-c-1.png)

## Formato de datos

__Ruido__ - Medición directa del voltaje del sensor de ruido multiplicada por 100 en 10 bits 

- voltaje de salida del sensor = ruido / 100 
 
__Humedad__ - La humedad está representada en porcentaje (0 - 100%) en los paquetes la medición viene multiplicada por 10 para poder integrar un decimal por lo que

- Humedad = valor de humedad en trama / 10

__Temperatura__ - La temperatura está representada en grados centígrados (°C). En la trama la medición viene multiplicada por 10 para poder integrar un decimal por lo que

- Temperatura= valor de temperatura en trama / 10

__PM 2.5__ - La medición de partículas de materia de 2.5µ está dada en unidades/cm3 y en la trama de datos viene almacenada en formato de número flotante (float - IEEE754) 

- PM 2.5 = (float) valor PM 2.5 en trama
			
__PM 10__ - La medición de partículas de materia de 10µ está dada en unidades/cm3 y en la trama de datos viene almacenada en formato de número flotante (float - IEEE754) 

- PM 10 = (float) valor PM 10 en trama

__NH3__ - La concentración del sensor NH3 está representada en partes por millón (PMM). En la trama se entrega la medición directa del voltaje del sensor multiplicada por 100 en 10 bits 

- voltaje de salida del sensor NH3 = valor NH3 en trama / 100 

__OX__ - La concentración del sensor OX está representada en partes por millón (PMM). En la trama se entrega la medición directa del voltaje del sensor multiplicada por 100 en 10 bits 

- voltaje de salida del sensor OX= valor OX en trama / 100 

__RED__ - La concentración del sensor RED está representada en partes por millón (PMM). En la trama se entrega la medición directa del voltaje del sensor multiplicada por 100 en 10 bits 

- voltaje de salida del sensor RED = valor RED en trama / 100 

## Vincular dispositivo con Árbol IoT 2.0

Para poder conectar tu dispositivo con el backend Árbol IoT 2.0 es necesario crear un callback de comunicación en tu dispositivo IoT, servicio IoT o en la consola SIGFOX en caso
de utilizar este servicio, para esto, considerar los siguientes pasos:

1. Crear un nuevo callback de tipo __REST POST__ que llame al endpoint __/api/devices/web-hook__ de tu instancia Árbol IoT 2.0
2. Considerar que este callback debe contar con un __'Authorization Header'__ de tipo __'Bearer'__, mismo que fue configurado en el apartado __‘Instalación / Árbol IoT Web / Contenedor Árbol IoT / Entorno / Sigfox’__ de esta guía. 
3. Considerar que el cuerpo de la petición deberá contener la siguiente estructura JSON:

    ```
    {
        "device":"{device}",
        "time":"{time}",
        "duplicate":"{duplicate}",
        "snr":"{snr}",
        "station":"{station}",
        "data":"{data}",
        "lat":"{lat}",
        "lng":"{lng}",
        "rssi":"{rssi}"
    }
    ```

4. Una vez sea puesto en marcha el dispositivo IoT y una vez este envíe su primer mensaje al backend Árbol 
	IoT 2.0, este último, reflejará al nuevo dispositivo en la consola y la nueva medición en el visor. 
     Podrás ver este nuevo dispositivo y estas nuevas mediciones como se indica en la __'Guía del 
	administrador general'__, apartado __'Sensores y mediciones'__ de esta guía.
